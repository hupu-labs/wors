#!/bin/bash

npm install -g yamljs

CIRCLECI_CACHE_DIR="${HOME}/bin"

PACKER_VERSION="0.12.1"
if [[ ! -e "$CIRCLECI_CACHE_DIR/packer" ]]; then
    PACKER_URL="https://releases.hashicorp.com/packer/${PACKER_VERSION}/packer_${PACKER_VERSION}_linux_amd64.zip"
    if [ ! -f "${CIRCLECI_CACHE_DIR}/packer" ]; then
        wget -O /tmp/packer.zip "${PACKER_URL}"
        unzip -d "${CIRCLECI_CACHE_DIR}" /tmp/packer.zip
    fi
    packer version
fi
