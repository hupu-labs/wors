#!/bin/sh

echo Getting minikube status...
STATUS="$(minikube status)"
echo "$STATUS"

echo "$STATUS" | awk '$2 != "Running" {exit 1}'
if [ $? -eq 1 ]; then
    minikube start --vm-driver xhyve --cpus 1 --memory 1024
    minikube addons enable ingress

    REPLACE="$(minikube ip) $SITE_URL"
    REGEX="s/(^[[:digit:]]{1,3}\.[[:digit:]]{1,3}\.[[:digit:]]{1,3}\.[[:digit:]]{1,3})? $SITE_URL/$REPLACE/g"
    echo Editing /etc/hosts, administrator privileges required
    sudo sed -E -i.bak "$REGEX" /etc/hosts
else
    echo Minikube already running
fi

minikube docker-env | sed 's/=/\ :=\ /g' | sed 's/"//g' | sed -E 's/(\#.*)//g' > .targets/minikube-env

# kubectl create secret docker-registry gcr-json-key \
#         --docker-server=https://gcr.io \
#         --docker-username=_json_key \
#         --docker-password='$(pass hupu/google/wors-roll/service-account-base64 | base64 --decode)' \
#         --docker-email=d.costaras@gmail.com

# kubectl patch serviceaccount default \
#         -p '{"imagePullSecrets": [{"name": "gcr-json-key"}]}'
