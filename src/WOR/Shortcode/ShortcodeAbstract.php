<?php namespace WOR\Shortcode;

abstract class ShortcodeAbstract {

    var $name;
    var $keys = [];

    public function __construct() {
        add_shortcode( $this->name, [ $this, 'shortcode' ] );
    }

    public function shortcode( $args, $content ) {
        throw new Exception( 'Function shortcode needs to be implemented in child class' );
    }

    protected function extract_shortcode_arguments( $args ) {
        $result = "";
        foreach ( $this->keys as $key ) {
            if ( isset( $args[ $key ] ) ) {
                $result .= $key . '="' . $args[ $key ] . '" ';
            }
        }
        return $result;
    }

}
