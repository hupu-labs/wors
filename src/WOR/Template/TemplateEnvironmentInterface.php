<?php
namespace WOR\Template;

interface TemplateEnvironmentInterface {

    public function __construct(
        $environment,
        $template_functions
    );

    public function render( $context );

}
