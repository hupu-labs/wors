<?php
namespace WOR\Template;

class Environment implements TemplateEnvironmentInterface {

    protected static $environment;
    private static $template_functions;

    public function __construct(
        $environment,
        $template_functions
    ) {
        self::$environment = $environment;
        self::$template_functions = $template_functions;
    }

    protected static function init() {
        throw new \Exception( 'You need to implement your own init() function.' );
    }

    public function render( $context = [] ) {

        if ( ! isset( $context[ 'template' ] ) ) {
            throw new \Exception( 'To render you need a template' );
        }

        $view_name = $context[ 'template' ];
        $context = array_merge( $context, self::$template_functions );
        $view_name = $view_name . '.twig';

        echo self::$environment->render( $view_name, $context );

    }

}
