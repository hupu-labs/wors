<?php
/**
 * Copyright (C) 2013-2015 by Donavan-Ross Costaras <d.costaras@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace WOR\Template;

class HamlExtension {

    public function register_with( \Aura\Di\Container $app ) {

        $app->set( 'template.functions', function () {
            return [
                'image' => new Functions\Image,
                'misc' => new Functions\Misc
            ];
        });


        $app->set( 'template.haml_environment', function () use ( $app ) {

            $mount = new \MtHaml\Environment( 'twig', [ 'enable_escaper' => false ] );
            $fs = new \Twig_Loader_Filesystem( $app->params[ 'views_folder' ] );
            $loader = new \MtHaml\Support\Twig\Loader( $mount, $fs );
            $twig = new \Twig_Environment( $loader, [ 'cache' => false, 'autoescape' => false ] );

            $twig->addExtension( new \MtHaml\Support\Twig\Extension );
            $twig->addExtension( new \Twig_Extensions_Extension_Text );

            if ( $app->has( 'image.twig_extension' ) ) {
                $twig->addExtension( $app->get( 'image.twig_extension' ) );
            }

            if ( $app->has( 'inflector.twig_extension' ) ) {
                $twig->addExtension( $app->get( 'inflector.twig_extension' ) );
            }

            if ( $app->has( 'autolink.twig_extension' ) ) {
                $twig->addExtension( $app->get( 'autolink.twig_extension' ) );
            }

            if ( $app->has( 'models.theme_options' ) ) {
                $twig->addGlobal( 'theme_options', $app->get( 'models.theme_options' ) );
            }

            $twig->addFunction( new \Twig_SimpleFunction( 'header', 'header' ) );
            $twig->addFunction( new \Twig_SimpleFunction( 'status_header', 'status_header' ) );
            $twig->addFunction( new \Twig_SimpleFunction( 'pp', 'pp' ) );

            $twig->addFunction( new \Twig_SimpleFunction( 'wp_head', 'wp_head' ) );
            $twig->addFunction( new \Twig_SimpleFunction( 'wp_footer', 'wp_footer' ) );
            $twig->addFunction( new \Twig_SimpleFunction( 'get_bloginfo', 'get_bloginfo') );

            $twig->addFunction( new \Twig_SimpleFunction( 'get_theme_mod', 'get_theme_mod' ) );

            $twig->addFilter( new \Twig_SimpleFilter( 'array', function ( $string ) {
                if ( is_serialized( $string ) ) {
                    return unserialize( $string );
                } elseif ( $array = json_decode( $string ) ) {
                    return $array;
                } else {
                    return (array) $string;
                }
            }));

            $twig->addFunction( new \Twig_SimpleFunction( 'array', 'unserialize' ) );
            $twig->addFunction( new \Twig_SimpleFunction( 'json', 'json_decode' ) );
            $twig->addFunction( new \Twig_SimpleFunction( 'to_json', 'json_encode' ) );

            $twig->addFunction( new \Twig_SimpleFunction( 'get_json_tags', [
                $app->get( 'term' ), 'get_json_tags'
            ]));

            return $twig;
        });

        $app->set( 'template', function () use ( $app ) {
            return new Environment(
                $app->get( 'template.haml_environment' ),
                $app->get( 'template.functions' )
            );
        });

    }
}
