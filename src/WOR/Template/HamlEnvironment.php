<?php
namespace WOR\Template;
class HamlEnvironment extends EnvironmentAbstract implements TemplateEnvironmentInterface {

    protected static function init() {
        $mount = new \MtHaml\Environment( 'twig', [ 'enable_escaper' => false ] );
        $fs = new \Twig_Loader_Filesystem( self::$views_folder );
        $loader = new \MtHaml\Support\Twig\Loader( $mount, $fs );
        $twig = new \Twig_Environment( $loader, [ 'cache' => false, 'autoescape' => false ] );
        $twig->addExtension( new \MtHaml\Support\Twig\Extension );
        $twig->addExtension( new \Twig_Extensions_Extension_Text );
        $twig->addExtension( new \WOR\Imagine\ImagineTwigExtension );
        $twig->addExtension( new \WOR\Inflector\InflectorTwigExtension );
        self::$environment = $twig;
    }

}
