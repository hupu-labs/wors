<?php
namespace WOR\dbg;
class Debug {

    private Static $_instance, $_then, $_hook_functions;

            function list_live_hooks( $hook = false ) {
            if ( false === $hook )
                $hook = 'all';
            add_action( $hook, Array($this, 'list_hook_details'), -1 );
        }

        function list_hook_details( $input = NULL ) {
            global $wp_filter;

            $tag = current_filter();
            if( isset( $wp_filter[$tag] ) )
                $this->dump_hook( $tag, $wp_filter[$tag] );

            return $input;
        }

        function list_hooks( $filter = false ){
            global $wp_filter;

            $hooks = $wp_filter;
            ksort( $hooks );

            foreach( $hooks as $tag => $hook )
                if ( false === $filter || false !== strpos( $tag, $filter ) )
                    $this->dump_hook($tag, $hook);
        }

        function dump_hook( $tag, $hook ) {
            /* ksort($hook); */

            echo "<pre>>>>>>\t$tag<br>";

            foreach( $hook as $priority => $functions ) {

                echo $priority;

                foreach( $functions as $function )
                    if( $function['function'] != 'list_hook_details' ) {

                        echo "\t";

                        /* var_dump( $function ); */
                        /* if( is_string( $function['function'] ) ) */
                        /*     echo $function['function']; */
                        /* elseif( is_string( $function['function'][0] ) ) */
                        /*     echo $function['function'][0] . ' -> ' . $function['function'][1]; */
                        /* elseif( is_object( $function['function'][0] ) ) */
                        /*     echo "(object) " . get_class( $function['function'][0] ) . ' -> ' . $function['function'][1]; */

                        /* else */
                        print_r($function);

                        echo ' (' . $function['accepted_args'] . ') <br>';
                    }
            }

            echo '</pre>';
        }

        public function __construct() {
            define( 'SAVEQUERIES', true );
            self::$_then = microtime(true);
        }

        public function mem_used() {
            echo "Final: " . memory_get_usage() . " bytes \n";
        }

        public function total_mem_used() {
            echo "Peak: " . memory_get_peak_usage() . " bytes \n";
        }

        public static function elapsed() {
            if ( defined( 'WP_DEBUG' ) )
                echo sprintf("Elapsed:  %f", microtime(true) - self::$_then);
        }

        public static function pp_wp() {
            global $wp;
            echo '<pre>';
            print_r($wp);
            echo '</pre>';
        }
        public static function flush_wp_rewrite() {
            flush_rewrite_rules();
        }

        private function __clone() {}

        public function pp($obj, $file) {
            echo '<pre>';
            echo $file;
            var_dump($obj);
            echo '</pre>';
        }

        public static function pp_filter( $text = '' ) {
            return static function( $var ) use ( $text ) {
                echo '<pre>';
                $tmp = [ 'text' => $text, 'var' => $var ];
                print_r($tmp);
                echo '</pre>';
                return $var;
            };
        }

        static function dump_filter() {
            $out=array_map(function($x) {return $x*$x;}, range(0, 9));
            echo '<ul>';
            foreach ( $GLOBALS['wp_filter'] as $tag => $priority_sets ) {
                echo '<li><strong>' . $tag . '</strong><ul>';
                foreach ( $priority_sets as $priority => $idxs ) {
                    echo '<li>' . $priority . '<ul>';
                    foreach ( $idxs as $idx => $callback ) {
                        if ( gettype($callback['function']) == 'object' ) $function = '{ closure }';
                        else if ( is_array( $callback['function'] ) ) {
                            $function = print_r( $callback['function'][0], true );
                            $function .= ':: '.print_r( $callback['function'][1], true );
                        }
                        else $function = $callback['function'];
                        echo '<li>' . $function . '<i>(' . $callback['accepted_args'] . ' arguments)</i></li>';
                    }
                    echo '</ul></li>';
                }
                echo '</ul></li>';
            }
            echo '</ul>';
        }
    }
