<?php
namespace WOR\Form;

class FormExtension  {

    public function register_with( \Aura\Di\Container $app ) {


        $app->set( 'form.factory', function () use ( $app ) {
            return $app->newInstance( 'WOR\Form\Factory' );
        });

        $app->set( 'form.fieldset.factory', function () use ( $app ) {
            return $app->newInstance( 'WOR\Form\FieldSet\Factory' );
        });

        $app->set( 'form.element.factory', function () use ( $app ) {
            return $app->newInstance( 'WOR\Form\Element\Factory' );
        });

        $app->params[ 'WOR\Form\Element\Option\Select\Sidebars' ][ 'sidebars' ] = $app->lazyGet( 'widget.area.factory' );
        $app->params[ 'WOR\Form\Element\Option\CptAbstract' ][ 'model_factory' ] = $app->lazyGet( 'model.factory' );
        $app->params[ 'WOR\Form\Element\Option\CptsAbstract' ][ 'model_factory' ] = $app->lazyGet( 'model.factory' );

        $app->params[ 'WOR\Form\FieldSet\Factory' ][ 'fieldsets' ] = [
            'repeatable' => $app->newFactory( 'WOR\Form\FieldSet\Repeatable' ),
        ];

        $app->params[ 'WOR\Form\Element\Factory' ][ 'inflector' ] = $app->lazyGet( 'inflector' );

        $app->params[ 'WOR\Form\Element\Factory' ][ 'fields' ] = [

            // Standards
            'checkbox'            => $app->newFactory( 'WOR\Form\Element\Checkbox' ),
            'text_area'           => $app->newFactory( 'WOR\Form\Element\TextArea' ),
            'hidden'              => $app->newFactory( 'WOR\Form\Element\Hidden' ),

            // Text field based
            'text'                => $app->newFactory( 'WOR\Form\Element\Text\Base' ),

            'date'                => $app->newFactory( 'WOR\Form\Element\Text\Date' ),
            'time'                => $app->newFactory( 'WOR\Form\Element\Text\Time' ),
            'date_time'           => $app->newFactory( 'WOR\Form\Element\Text\DateTime' ),
            'url'                 => $app->newFactory( 'WOR\Form\Element\Text\Url' ),
            'duration'            => $app->newFactory( 'WOR\Form\Element\Text\Duration' ),
            'youtube_id'          => $app->newFactory( 'WOR\Form\Element\Text\YouTubeId' ),
            'number'              => $app->newFactory( 'WOR\Form\Element\Text\Number' ),
            'colour'              => $app->newFactory( 'WOR\Form\Element\Text\Colour' ),

            // Js library based
            'editor'              => $app->newFactory( 'WOR\Form\Element\Editor' ),
            'file'                => $app->newFactory( 'WOR\Form\Element\File' ),
            'colour_scheme'       => $app->newFactory( 'WOR\Form\Element\ColourScheme' ),
            'gallery'             => $app->newFactory( 'WOR\Form\Element\Gallery' ),
            'google_map_address'  => $app->newFactory( 'WOR\Form\Element\GoogleMapAddress' ),
            'google_map_gps'      => $app->newFactory( 'WOR\Form\Element\GoogleMapGps' ),
            'google_map'          => $app->newFactory( 'WOR\Form\Element\GoogleMap' ),

            // Option based
            'radio'               => $app->newFactory( 'WOR\Form\Element\Option\Radio' ),

            'select'              => $app->newFactory( 'WOR\Form\Element\Option\Select\Base' ),
            'multiple_select'     => $app->newFactory( 'WOR\Form\Element\Option\MultipleSelect\Base' ),

            'sidebars'            => $app->newFactory( 'WOR\Form\Element\Option\Select\Sidebars' ),
            'post_type'           => $app->newFactory( 'WOR\Form\Element\Option\Select\PostType' ),

            'category'            => $app->newFactory( 'WOR\Form\Element\Option\Select\Categories' ),
            'multiple_categories' => $app->newFactory( 'WOR\Form\Element\Option\MultipleSelect\Categories' ),

            'taxonomy'            => $app->newFactory( 'WOR\Form\Element\Option\Select\Taxonomy' ),
            'multiple_taxonomies' => $app->newFactory( 'WOR\Form\Element\Option\MultipleSelect\Taxonomy' ),

            'cpts' => $app->newFactory( 'WOR\Form\Element\Option\Select\Cpts' ),
            'multiple_cpts' => $app->newFactory( 'some class' ),

            'cpt' => $app->newFactory( 'WOR\Form\Element\Option\Select\Cpt' ),
            'multiple_cpt' => $app->newFactory( 'WOR\Form\Element\Option\MultipleSelect\Cpt' )
        ];

    }
}
