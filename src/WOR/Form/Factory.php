<?php
namespace WOR\Form;

class Factory {

    public function create( $args ) {
        return new Form( $args );
    }

}
