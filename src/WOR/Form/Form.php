<?php
namespace WOR\Form;
use \Functional as F;
use \Partial as P;

class Form implements FormInterface, \IteratorAggregate, \ArrayAccess {

    private $products;

    function __construct( Array $form_elements = [] ) {

        foreach ( $form_elements as $ele ) {
            $this->products[ $ele->slug() ] = $ele;
        }

    }

    public function set_decorator( $decorator_class ) {

        $decorator_class = __NAMESPACE__ . '\\Decorator\\' . $decorator_class;

        $this->products = F\map( $this->products, function ( $ele ) use ( $decorator_class ) {
            return new $decorator_class( $ele );
        });

        return $this;

    }

    public function set_prefix( $prefix ) {

        $this->prefix = $prefix;

        foreach ( $this->products as $ele ) {
            $ele->set_prefix( $prefix );
        }

        return $this;
    }

    public function get_prefix() {
        return isset( $this->prefix ) ? $this->prefix : '';
    }

    public function set_values_from_post() {

        if ( isset( $_POST[ $this->get_prefix() ] ) ) {
            $this->set_values( $_POST[ $this->get_prefix() ] );
        }

        return $this;
    }

    public function set_values( $values ) {

        if ( ! empty( $values ) ) {

            F\each( $this->products, function ( $e ) use ( $values ) {

                $key = $e->slug();

                if ( array_key_exists( $key, $values ) ) {
                    $e->set_value( $values[ $key ] );
                }

            });

        }

        return $this;

    }

    function get_headings() {
        return 'asdf';
    }

    function set_index( $index ) {

        foreach ( $this->products as $field ) {
            $field->set_index( $index );
        }

    }

    function add_hidden( $check, $hidden_fields ) {
        $this->products[ $check ]->set_class( 'hidden_check' );
        foreach ( $this->products as $field ) {
            foreach ( $hidden_fields as $hidden_field ) {
                if ( $field->slug() == $hidden_field ) {
                    $field->set_class( 'hidden_field hidden' );
                    break;
                }
            }
        }
        return $this;
    }

    function __toString() {
        return F\reduce_left( $this->products, 'concat_string' );
    }

    public function __get( $name ) {
        return $this->products[ $name ]->value();
    }

    public function getIterator() {
        return new \ArrayIterator( $this->products );
    }

    public function offsetSet( $id, $value ) {
        $this->products[ $id ] = $value;
    }

    public function offsetGet( $id ) {
        if ( ! array_key_exists( $id, $this->products ) ) {
            throw new InvalidArgumentException( sprintf( 'Form element, "%s", is not defined.', $id ) );
        }

        return $this->products[ $id ];
    }

    public function offsetExists( $id ) {
        return array_key_exists( $id, $this->products );
    }

    public function offsetUnset( $id ) {
        unset( $this->products[ $id ] );
    }

    public function to_array( $predicate = false ) {

        if ( $predicate ) {

            return F\map( F\filter( $this->products, $predicate ), function ( $e ) {
                return $e->value() ?: false;
            });

        } else {

            return F\map( $this->products, function ( $e ) {
                return $e->value() ?: false;
            });

        }


    }

}
