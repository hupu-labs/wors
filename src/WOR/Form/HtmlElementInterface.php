<?php
namespace WOR\Form;

interface HtmlElementInterface {
    public function __toString();
}
