<?php
namespace WOR\Form\Element;

class Hidden extends ConcreteFormElement implements FormElementInterface {

    function __toString() {

        return (
            '<input type="text" ' .
            $this->id_attr() .
            $this->class_attr() .
            $this->name_attr() .
            $this->value_attr() .
            $this->customizer_setting() .
            ' />'
        );

    }

    function get_escaper() {
        return function ( $value ) {
            return $value;
        };
    }

    function get_sanitizer() {
        return function ( $value ) {
            return $value;
        };
    }
}
