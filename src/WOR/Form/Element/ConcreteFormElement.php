<?php
/**
 * Copyright (C) 2013-2015 by Donavan-Ross Costaras <d.costaras@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace WOR\Form\Element;


abstract class ConcreteFormElement implements FormElementInterface {

    protected $value = null;
    protected $customizer_setting = false;
    protected $extra_classes = false;
    protected $sanitizer;
    protected $escaper;

    protected $type;
    protected $name;
    protected $title;
    protected $standard;
    protected $description;
    protected $class;
    protected $options;
    protected $admin;
    protected $prefix;

    public function __construct( $args ) {

        $this->type = $args[ 'type' ];
        $this->name = $args[ 'name' ];
        $this->title = is( $args[ 'title' ], '' );
        $this->standard = is( $args[ 'standard' ] );
        $this->description = is( $args[ 'description' ], '' );
        $this->set_class( is( $args[ 'class' ], '' ) );
        $this->options = is( $args[ 'options' ] );
        $this->admin = is( $args[ 'admin' ] );
        $this->size = is( $args[ 'size' ] );
        $this->escaper = is( $args[ 'escaper' ], $this->get_escaper() );
        $this->sanitizer = $this->get_sanitizer();

    }

    public function set_sanitizer( $sanitizer ) {
        $this->sanitizer = $sanitizer;
    }

    public function set_escaper( $escaper ) {
        $this->escaper = $escaper;
    }

    public function set_repeater_index( $repeater_index ) {
        $this->repeater_index = $repeater_index;
    }

    public function set_customizer_setting( $setting_link ) {
        $this->customizer_setting = $setting_link;
    }

    public function customizer_setting( $attr = 'data-customize-setting-link' ) {

        if ( $this->customizer_setting ) {
            return (
                $attr . '="' .
                $this->customizer_setting .
                '" '
            );
        } else {
            return '';
        }

    }

    public function enqueue() {}

    public function name() {

        return (
            $this->prefix .
            ( isset( $this->index ) ? '[' . $this->index . ']' : '' ) .
            ( isset( $this->repeater_slug ) ? '[' . $this->repeater_slug . ']' : '' ) .
            ( isset( $this->repeater_index ) ? '[' . $this->repeater_index. ']' : '' ) .
            '[' . $this->name . ']'
        );

    }

    public function description() {
        return $this->description;
    }

    public function slug() {
        // throw new \Exception('slug method is deprecated');
        return $this->name;
    }

    public function id_attr() {
        return (
            'id="' .
            esc_attr( $this->slug() ) .
            '" '
        );
    }


    protected function size() {
        return $this->size;
    }

    public function size_attr( $value = null ) {

        return (
            'size="' .
            esc_attr( $value ?: $this->size() ) .
            '" '
        );

    }

    public function standard() {
        return $this->standard;
    }

    public function title() {
        return $this->title;
    }

    public function klass() {
        return $this->class;
    }

    public function class_attr( $extra_class = null ) {
        return (
            'class="' .
            esc_attr( $this->klass() ) .
            ' ' .
            ( $extra_class ?: '' ) .
            '" '
        );
    }

    public function name_attr() {
        return (
            'name="' .
            esc_attr( $this->name() ) .
            '" '
        );
    }

    public function value_attr( $value = null ) {

            return (
                'value="' .
                esc_attr( $value ?: $this->value() ) .
                '" '
            );

        // if ( $value ) {


        // } else {

        //     return (
        //         'value="' .
        //         esc_attr( $this->value() ) .
        //         '" '
        //     );

        // }

    }

    public function set_index( $index ) {
        $this->index = $index;
    }


    public function prefix() {
        return $this->prefix;
    }

    public function set_prefix( $prefix ) {
        $this->prefix = $prefix;
    }

    public function set_value( $value ) {
        $sanitizer = $this->sanitizer;
        $this->value = $sanitizer( $value );
        return $this;
    }

    protected function esc( $value ) {
        $escaper = $this->escaper;
        return $escaper( $value );
    }

    public function set_class( $class ) {

        $this->class = $class;

        if ( $this->extra_classes ) {
            $this->class .= ' ' . $this->extra_classes;
        }

        return $this;
    }

    public function value() {

        if ( isset( $this->value ) ) {
            return $this->esc( $this->value );
        } else {
            return $this->esc( $this->standard );
        }

    }

    public function get_index() {
        return $this->index;
    }

    // function with_prefix( $slug, $type = null ) {
    //     return (
    //         $this->prefix .
    //         '[' . $type . '_' . $slug . ']'
    //     );
    // }

}
