<?php
namespace WOR\Form\Element\Option;

use WOR\Form\Element\ConcreteFormElement;
use \Functional as F;

abstract class OptionFormElementAbstract extends ConcreteFormElement implements OptionFormElementInterface {

    abstract function __toString();

    protected function get_wrap() {
        return function ( $val, $key, $options, $acc ) {
            return $acc . (
                "\t\t<option " .
                $this->value_attr( $this->esc( $key ) ) .
                $this->selected_attr( $key ) .
                '>' .
                $this->esc( $val ) .
                "</option>\n"
            );
        };
    }

    protected function selected_attr( $key ) {
        return ( $this->value() === (String) $key ) ?
            ' selected="selected"' :
            '';
    }

    protected function foldl_options() {
        if ( $this->options ) {
            return F\reduce_left( $this->options, $this->get_wrap() );
        } else {
            return false;
        }
    }

    protected function get_sanitizer() {
        return function ( $value ) {
            $value = esc_sql( strip_tags( $value ) );
            if ( array_key_exists( $value, $this->options ) ) {
                return $value;
            } else {
                return '';
            }
        };
    }

    protected function get_escaper() {
        return function ( $value ) {
            // pp($value);
            return strip_tags( $value );
        };
    }
}
