<?php
namespace WOR\Form\Element\Option;

class Radio extends OptionFormElementAbstract implements OptionFormElementInterface {

    public function __toString() {
        return $this->foldl_options();
    }

    function get_wrap() {
        return function ( $val, $key, $options, $acc ) {
            return $acc . (
                "<input type='radio' " .
                $this->name_attr() .
                $this->id_attr() .
                $this->value_attr( $this->esc( $key ) ) .
                $this->selected_attr( $key ) .
                $this->customizer_setting() .
                "/>" .
                " " .
                $this->esc( $val ) .
                "<br />"
            );
        };
    }

    protected function selected_attr( $key ) {
        return ( $this->value() === (String) $key ) ?
            ' checked="checked"' :
            '';
    }

}
