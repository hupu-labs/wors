<?php
namespace WOR\Form\Element\Option\MultipleSelect;
use WOR\Form\Element\Option\TaxonomyAbstract;

class Taxonomy extends TaxonomyAbstract {

    use MultipleSelectTrait;

}
