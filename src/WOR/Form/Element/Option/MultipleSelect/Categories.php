<?php
namespace WOR\Form\Element\Option\MultipleSelect;
use WOR\Form\Element\Option\CategoriesAbstract;

class Categories extends CategoriesAbstract {

    use MultipleSelectTrait;

}
