<?php
namespace WOR\Form\Element\Option\MultipleSelect;

trait MultipleSelectTrait {

    public function __toString() {
        return (
            '<select name="' .
            $this->name() . '[]' .
            '" style="width: 100%' .
            '" class="widefat multiple-select" multiple>' .
            $this->foldl_options() .
            '</select>'
        );
    }

}
