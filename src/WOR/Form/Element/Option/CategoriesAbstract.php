<?php
namespace WOR\Form\Element\Option;

abstract class CategoriesAbstract extends OptionFormElementAbstract {

    public function __construct( $args ) {
        $args[ 'options' ] = get_all_categories();
        parent::__construct( $args );
    }

}
