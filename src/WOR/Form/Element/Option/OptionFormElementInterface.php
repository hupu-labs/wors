<?php
namespace WOR\Form\Element\Option;
use WOR\Form\Element\FormElementInterface;

interface OptionFormElementInterface extends FormElementInterface {

    public function __toString();

}
