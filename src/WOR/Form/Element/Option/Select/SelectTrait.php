<?php
namespace WOR\Form\Element\Option\Select;

trait SelectTrait {

    public function __toString() {

        return (
            "\t<select " .
            $this->name_attr() .
            $this->customizer_setting() .
            ">\n" .
            $this->foldl_options() .
            "\t</select>\n"
        );

    }

}
