<?php
namespace WOR\Form\Element\Option;

abstract class TaxonomyAbstract extends OptionFormElementAbstract {

    public function __construct( $args ) {
        $args[ 'options' ] = get_all_terms_from_taxonomy( $args[ 'name' ] );
        parent::__construct( $args );
    }

}
