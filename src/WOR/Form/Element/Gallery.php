<?php
/**
 * Copyright (C) 2013-2015 by Donavan-Ross Costaras <d.costaras@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
namespace WOR\Form\Element;

use \Functional as F;

class Gallery extends ConcreteFormElement implements FormElementInterface {

    public function __toString() {
        return (
            '<style>
                .gallery_image{ width: 100px; height: 100px }
                .gallery-input{ display:none; }
                .gallery_sortable{ list-style-type: none; margin: 0; padding: 0; width: 450px; clear:both; }
                .gallery_sortable li { margin: 3px 3px 3px 0; padding: 1px; float: left; width: 100px; height: 90px; font-size: 4em; text-align: center; }
            </style>' .

            /* This input tag uses single quotes in the HTML as it's value is JSON */
            "<input type='text' class='gallery-input' value='" .
            $this->value() .
            "' name='" .
            $this->name() .
            "' />" .

            '<ul class="gallery_sortable">' .
            $this->get_inner_li() .
            '</ul>' .

            '<br class="clear"/>' .

            '<a class="button wp_upload_custom" href="#" style="margin-top: 20px;">Select or Upload Images</a>'
        );
    }

    private function get_wrap() {
        return function ( $element, $index, $collection, $acc ) {
            return $acc . (
                '<li><img class="gallery_image" src="' .
                $element->url .
                '" id="' .
                $element->id .
                '">' .
                '</li>'
            );
        };
    }

    private function get_inner_li() {
        return F\reduce_left( json_decode( $this->value() ), $this->get_wrap() );
        // return foldl( json_decode( $this->value() ), '', $this->get_wrap() );
    }

    public function get_escaper() {
        return function ( $value ) {
            return $value ;
        };
    }

    public function get_sanitizer() {
        return function ( $value ) {
            return $value;
        };
    }

}
