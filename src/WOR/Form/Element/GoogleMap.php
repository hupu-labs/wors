<?php
/**
 * Copyright (C) 2013-2015 by Donavan-Ross Costaras <d.costaras@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace WOR\Form\Element;

class GoogleMap extends ConcreteFormElement implements FormElementInterface {
    var $extra_classes = 'address_input';

    public function __toString() {
        return (

            '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDq2BglyL9xxOmKa2USnZ1WraUzB3J3noY&libraries=places&sensor=false" type="text/javascript"></script>' .

            // '<input type="hidden" style="width:300px;" ' .
            // $this->id_attr() .
            // $this->class_attr( 'address_input_select' ) .
            // $this->name_attr() .

            // // holds JSON, use ' in HTML
            // "value='" .
            // $this->value() .
            // "'" .

            // ' />' .

            '<div id="map-canvas" ></div>'
        );
    }

    public function get_escaper() {
        return function ( $value ) {
            return $value ;
        };
    }

    public function get_sanitizer() {
        return function ( $value ) {
            return $value;
        };
    }

}
