<?php
/**
 * Copyright (C) 2013-2015 by Donavan-Ross Costaras <d.costaras@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace WOR\Form\Element;

class Editor extends ConcreteFormElement implements FormElementInterface {



    public function __toString() {

        return (
            '<script type="text/javascript">' .
            'jQuery(document).ready(function() {' .

            '    jQuery("#' . $this->slug() . '").addClass("mceEditor");' .

            '    tinymce.init({' .
            '        selector: "#' . $this->slug() . '",' .
            '    });' .

            '});' .
            '</script>' .

            '<textarea ' .
            $this->id_attr() .
            'rows="15" cols="80" style="width: 100%; height: 300px;" ' .
            $this->class_attr() .
            $this->name_attr() .
            '>' .
            $this->value() .
            '</textarea>'
        );
    }

    public function get_escaper() {
        return function ( $value ) {
            return $value;
            /* return strip_tags( $value ); */
        };
    }

    public function get_sanitizer() {
        return function ( $value ) {
            return $value;
            // return sanitize_text_field( $value );
        };
    }

}
