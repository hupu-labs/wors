<?php
namespace WOR\Form\Element\Text;
use \WOR\Form\Element\ConcreteFormElement;
use \WOR\Form\Element\FormElementInterface;

class Duration extends Base implements FormElementInterface {

    var $extra_classes = 'duration-picker';

    public function get_escaper() {
        return function ( $value ) {
            return $value;
            /* return strip_tags( $value ); */
        };
    }

    public function get_sanitizer() {
        return function ( $value ) {
            return $value;
            /* return sanitize_text_field( $value ); */
        };
    }

}
