<?php
namespace WOR\Form\Element\Text;
use \WOR\Form\Element\ConcreteFormElement;
use \WOR\Form\Element\FormElementInterface;

class Date extends Base implements FormElementInterface {

    var $extra_classes = 'date-picker';

    public function get_escaper() {
        return function ( $value ) {

        };
    }

    public function get_sanitizer() {
        return function ( $value ) {

            $test_arr  = explode( '/', $value );

            if ( count( $test_arr ) === 3 ) {

                if ( checkdate( $test_arr[1], $test_arr[2], $test_arr[0] ) ) {
                    return $value;
                } else {
                    return '';
                }

            } else {
                return '';
            }

            return $value;

        };
    }

}
