<?php
namespace WOR\Form\Element\Text;

class Colour extends Base {

    var $extra_classes = 'color-picker-hex';

    public function enqueue() {
        wp_enqueue_script( 'wp-color-picker' );
        wp_enqueue_style( 'wp-color-picker' );
    }

}
