<?php
namespace WOR\Form\Element\Text;
use \WOR\Form\Element\ConcreteFormElement;
use \WOR\Form\Element\FormElementInterface;

class DateTime extends Base implements FormElementInterface {

    var $extra_classes = 'date-time-picker';

    public function get_escaper() {
        return function ( $value ) {
            return ( new \DateTime( $value ) )->format( 'Y-m-d H:i:s' );
        };
    }

    public function get_sanitizer() {
        return function ( $value ) {

            $date = \DateTime::createFromFormat( 'Y-m-d H:i:s', $value );

            if ( $date ) {

                return $date->format( 'Y-m-d H:i:s' );

            } else {

                if ( $date = strtotime( $value ) !== false ) {
                    return $date;
                } else {
                    return '';
                }

            }

        };
    }

}
