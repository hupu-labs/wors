<?php
namespace WOR\Form\Element\Text;
use WOR\Form\Element\ConcreteFormElement;
use WOR\Form\Element\FormElementInterface;

class Base extends ConcreteFormElement implements FormElementInterface {

    public function __toString() {
        return (
            "<input type='text' " .
            $this->id_attr() .
            $this->class_attr() .
            $this->name_attr() .
            $this->value_attr() .
            $this->size_attr() .
            $this->customizer_setting() .
            " />"
        );
    }

    public function get_escaper() {
        return function ( $value ) {
            return esc_html( $value );
        };
    }

    public function get_sanitizer() {
        return function ( $value ) {
            return sanitize_text_field( $value );
        };
    }

}
