<?php
/**
 * Copyright (C) 2013-2015 by Donavan-Ross Costaras <d.costaras@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace WOR\Form\Element\Text;

use \WOR\Form\Element\ConcreteFormElement;
use \WOR\Form\Element\FormElementInterface;

class YouTubeId extends Base implements FormElementInterface {

    public function get_escaper() {
        return function ( $value ) {
            if ( preg_match('@^[^\&\?/]{11}$@', $value ) > 0 ) {
                return $value;
            } else {
                return '';
            }
        };
    }

    public function get_sanitizer() {
        return function ( $value ) {

            if ( preg_match('@^[^\&\?/]{11}$@', $value ) > 0 ) {

                return $value;

            } else {

                if ( preg_match( '@youtube(?:-nocookie)?\.com/watch\?v=([^\&\?/]{11})@', $value, $matches )) {
                    return $matches[1];
                } else if ( preg_match( '@youtube(?:-nocookie)?\.com/embed/([^\&\?/]{11})@', $value, $matches )) {
                    return $matches[1];
                } else if ( preg_match( '@youtube(?:-nocookie)?\.com/v/([^\&\?/]{11})@', $value, $matches )) {
                    return $matches[1];
                } else if ( preg_match( '@youtu\.be/([^\&\?/]{11})@', $value, $matches )) {
                    return $matches[1];
                } else if ( preg_match( '@youtube(?:-nocookie)?\.com/ytscreeningroom\?v=([^\&\?/]{11})@', $value, $matches )) {
                    return $matches[1];
                } else if ( preg_match( '@youtube(?:-nocookie)?\.com/user/.*/([^\&\?/]{11})@', $value, $matches )) {
                    return $matches[1];
                } else {
                    return false;
                }

            }

        };
    }

}
