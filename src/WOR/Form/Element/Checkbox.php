<?php
/**
 * Copyright (C) 2013-2015 by Donavan-Ross Costaras <d.costaras@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace WOR\Form\Element;

class Checkbox extends ConcreteFormElement implements FormElementInterface {

    function __toString() {

        return (
            // This hidden field is a hack to fix metaboxes. If the form is not
            // checked it doesn't end up in the $_POST array and is not removed
            // by updating the meta field in the db.
            '<input type="hidden" ' .
            $this->name_attr() .
            'value="0" />' .

            '<input type="checkbox" ' .
            $this->name_attr() .
            $this->class_attr() .
            $this->id_attr() .
            $this->value_attr( 1 ) .
            ( ( $this->value() === 1 ) ? ' checked' : '' ) .
            ' /> ' .
            $this->description()
        );

    }

    function get_escaper() {
        return function ( $value ) {
            return (Int) $value;
        };
    }

    function get_sanitizer() {
        return function ( $value ) {
            return (Int) $value;
        };
    }
}
