<?php
namespace WOR\Form\Element;

class TextArea extends ConcreteFormElement implements FormElementInterface {
    public function __toString() {
        return (
            '<textarea ' .
            $this->id_attr() .
            $this->name_attr() .
            'rows="15" cols="80" style="width: 100%; height: 300px;" ' .
            '>' .
            $this->value() .
            '</textarea>'
        );
    }

    public function get_escaper() {
        return function ( $value ) {
            return $value;
            /* return esc_html( $value ); */
        };
    }

    public function get_sanitizer() {
        return function ( $value ) {
            return wp_kses_post( $value );
        };
    }
}
