<?php
namespace WOR\Form\FieldSet;

class CheckBox extends FieldSet {

    public function __construct( $args ) {
        extract( $args );
        parent::__construct( $name, $title, $fields );
        $this->index = $name;

        /* array_map( */
        /*     function ( $ele ) use ( $name ) { */
        /*         return $ele->index = $name; */
        /*     }, */
        /*     $this->_element */
        /* ); */

        $this->_element[ 0 ]->set_class( 'checkbox-field_set-' . $name );
        /* $this->checkbox->index = $name; */

    }

    private function get_inner() {
        /* return array_reduce( */
        /*     $this->_element, */
        /*     function ( $string, $element ) { */
        /*         return $string . new \WOR\Form\Decorator\AdminDecorator( $element ); */
        /*     } */
        /* ); */
        $out = '';
        $count = count( $this->_element );
        for ( $i = 1; $i < $count; $i++ ) {
            $out .=  $this->_element[ $i ];
        }
        return $out;
    }

    public function __toString() {
        /* $checkbox =  */
        return (
            $this->_element[ 0 ] .
            '<div style="display:one;" class="checkbox-field_set-fields">' .
            $this->get_inner() .
            '</div>'
        );
    }

    public function set_value( $values ) {
        /* $this->checkbox->set_value( $values[ $this->checkbox->slug() ] ); */
        foreach ( $this->_element as $field ) {
            $field->set_value( $values );
        }
    }

    public function set_prefix( $prefix ) {
        $this->prefix = $prefix;
        /* $this->checkbox->set_prefix( $prefix ); */

        array_map(
            function ( $ele ) use ( $prefix ) { return $ele->set_prefix( $prefix ); },
            $this->_element
        );

        return $this;
    }
}
