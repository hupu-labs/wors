<?php
namespace WOR\Form\FieldSet;

class Factory {

    protected $fieldsets = [];

    public function __construct( $fieldsets = [] ) {
        $this->fieldsets = $fieldsets;
    }

    public function create( $args = [] ) {
        $factory = $this->fieldsets[ $args[ 'type' ] ];
        return $factory( $args );
    }

}
