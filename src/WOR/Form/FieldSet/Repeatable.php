<?php
namespace WOR\Form\FieldSet;
use \Functional as F;

class Repeatable extends FieldSet implements \WOR\Form\HtmlElementInterface {

    public function __construct( $args ) {

        parent::__construct(
            $args[ 'name' ],
            $args[ 'title' ],
            is( $args[ 'class' ] ),
            $args[ 'fields' ]
        );

        foreach ( $this->fields as $i => &$field ) {
            $field->repeater_slug = $args[ 'name' ];
            $field->repeater_index = 0;
        }

    }

    public function set_value( $values ) {
        $this->values = $values;
    }

    private function li_wrap( $val ) {
        return (
            '<li class="menu-item-page nav-menus-php menu-item-edit-inactive repeatable">' .

            '  <dl class="menu-item-bar">' .
            '    <dt class="menu-item-handle">' .

            '      <span class="item-title sort hndle">' .
            '        <span class="menu-item-title">' .
            '          <a class="repeatable-remove button" href="#">-</a> ' .
            $this->title .
            '        </span>' .
            '      </span>' .

            '      <span class="item-controls">' .
            '        <a class="item-edit" href="#">Toggle</a>' .
            '      </span>' .

            '    </dt>' .
            '  </dl>' .

            '  <div class="menu-item-settings">' .
            $val .
            '  </div>' .

            '</li>'
        );
    }

    public function __toString() {
        return (
            '<ul id="' . $this->slug . '-repeatable" class="custom_repeatable">' .
            $this->get_inner() .
            '</ul>' .

            '<a class="repeatable-add button" href="#">+</a> Add a new ' . $this->title .

            '<br />' .

            '<a class="repeatable-open-all" href="#">Open all</a>' .
            '<a class="repeatable-close-all hidden" href="#">Close all</a>'
        );
    }

    private function get_inner() {

        $out = '';

        if ( isset( $this->values ) ) {

            $i = 0;

            /* This is bit of a hack, metaboxes and widgets return different data */
            if ( ! is_array( $this->values ) ) {
                $this->values = unserialize( $this->values );
            }

            foreach ( $this->values as $row ) {

                $inner = '';

                foreach ( $this->fields as $field ) {

                    $val = is( $row[ $field->slug() ], '');

                    $field->set_value( $val )
                          ->set_repeater_index( $i );

                    $inner .= new \WOR\Form\Decorator\AdminDecorator( $field );

                }

                $out .= $this->li_wrap( $inner );

                $i++;

            }

        } else {

            $inner = '';

            foreach ( $this->fields as $field ) {
                $inner .= new \WOR\Form\Decorator\AdminDecorator( $field );
            }

            $out .= $this->li_wrap( $inner );
        }

        return $out;

    }

}
