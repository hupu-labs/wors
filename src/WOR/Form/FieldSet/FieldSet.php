<?php
namespace WOR\Form\FieldSet;

abstract class FieldSet implements \WOR\Form\HtmlElementInterface {

    public function __construct( $name, $title, $class, $fields ) {
        $this->slug = sanitize_title( $name );
        $this->class = $class;
        $this->name = $name;
        $this->title = $title;
        $this->fields = $fields;
    }

    public function set_value( $values ) {
        foreach ( $this->fields as $field ) {
            $field->set_value( $values );
        }
    }

    public function set_index( $index ) {
        foreach ( $this->fields as $field ) {
            $field->set_index( $index );
        }
    }

    public function value() {
        return $this->values;
    }

    // public function set_value( $values ) {
    //     $this->values = $values;
    // }

    public function klass() {
        return $this->class;
    }

    public function set_prefix( $prefix ) {

        $this->prefix = $prefix;

        foreach ( $this->fields as $field ) {
            $field->set_prefix( $prefix );
        }

        return $this;

    }

    public function title() {
        return $this->title;
    }

    public function name() {
        return $this->slug;
    }

    public function slug() {
        return $this->slug;
    }

    /* public function value() { */
    /*     $values = []; */
    /*     foreach ( $this->fields as $field ) { */
    /*         $values = $field->value(); */
    /*     } */
    /*     return $values; */
    /* } */

    public function __toString() {
        throw new \Exception( '__toString needs to be implemented in sub-class' );
    }

}
