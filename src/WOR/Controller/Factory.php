<?php
namespace WOR\Controller;

class Factory {

    protected $controllers = [];

    public function __construct(
        $controllers = [],
        $menu_factory,
        $sidebar_factory,
        $model_factory,
        $theme_options,
        $inflector
    ) {
        $this->controllers = $controllers;
        $this->menu_factory = $menu_factory;
        $this->sidebar_factory = $sidebar_factory;
        $this->model_factory = $model_factory;
        $this->theme_options = $theme_options;
        $this->inflector = $inflector;
    }

    public function create( $args ) {

        $factory = $this->controllers[ $args ];

        return $factory(
            $this->menu_factory,
            $this->sidebar_factory,
            $this->model_factory,
            $this->theme_options,
            $this->inflector
        );

    }


}
