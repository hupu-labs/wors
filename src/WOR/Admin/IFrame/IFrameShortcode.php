<?php namespace WOR\Admin\IFrame;

class IFrameShortcode extends \WOR\Shortcode\ShortcodeAbstract {

    var $name = 'iframe';
    var $keys = ['src', 'width', 'height', 'scrolling', 'marginwidth', 'marginheight', 'frameborder'];

    public function shortcode( $args, $content ) {
        $args = $this->extract_shortcode_arguments( $args );
        return '<iframe ' . $args . '></iframe>';
    }

}
