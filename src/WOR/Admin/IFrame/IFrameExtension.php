<?php namespace WOR\Admin\IFrame;

class IFrameExtension  {

    public function register_with( \Aura\Di\Container $app ) {

        $app->set( 'iframe.shortcode', function () use ( $app ) {
            return $app->newInstance( 'WOR\Admin\IFrame\IFrameShortcode' );
        });

        $app->set( 'iframe.widget', function () use ( $app ) {

            $form = $app->get( 'form.factory' )->create([

                // Some form elements

            ])->set_decorator( 'ShortAdminDecorator' );

            return $app->get( 'widget.factory' )->create([
                'template' => 'widget/iframe',
                'class' => '\Award\Widget\IFrame',
                'form' => $form,
                'name' => 'IFrame',
                'description' => 'Some description',
            ]);

        });

    }

}
