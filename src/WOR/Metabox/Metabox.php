<?php
namespace WOR\Metabox;
use Functional as F;

class Metabox implements MetaboxInterface {
    private $form;
    private $post_type;
    private $view;
    private $post_name;
    private static $template;
    private static $inflector;

    public function __construct(
        $name,
        $post_type,
        $view,
        \WOR\Form\FormInterface $form,
        \WOR\Template\TemplateEnvironmentInterface $template,
        \WOR\Inflector\InflectorInterface $inflector
    ) {
        $this->name = (String) $name;
        $this->post_type = (String) $post_type;
        $this->view = (String) $view;
        $this->form = $form;
        self::$inflector = $inflector;
        self::$template = $template;
    }

    public function set_post_name( $post_name ) {
        $this->post_name = (String) $post_name;
        return $this;
    }

    public function add_actions() {

        // If $this->post_name is set then we're only loading the box on certain edit pages for $this->post_type.
        if ( isset( $this->post_name ) ) {

            // This is called here as the post_id doesn't seem to be present on the post/redirect/get cycle.
            add_action( "save_post_{$this->post_type}", [ $this, 'save_box' ] );

            // Try find the current id, if any.
            if ( isset( $_GET[ 'post' ] ) ) {
                $post_id = $_GET[ 'post' ];
            } elseif ( isset( $_POST[ 'post' ] ) ) {
                $post_id = $_POST[ 'post' ];
            }

            // Check if we have an id and if it's the id we're looking for.
            if ( isset( $post_id ) && $post_id == get_id_from_slug( $this->post_name ) ) {

                // Show the metabox.
                add_action( 'add_meta_boxes', [ $this, 'add_box' ] );

            } else {

                // The request either doesn't have a post id or it's not the one we want. Bail.
                return;

            }

        } else {

            // We're loading the metabox on all posts of $this->post_type.
            add_action( 'add_meta_boxes', [ $this, 'add_box' ] );
            add_action( "save_post_{$this->post_type}", [ $this, 'save_box' ] );

        }
    }

    public function add_box() {
        add_meta_box(
            $this->name,
            self::$inflector->humanize( $this->name ),
            [ $this, 'display' ],
            $this->post_type,
            'normal',
            'high'
        );
    }

    private function get_metadata( $id ) {

        return F\map( get_post_meta( $id ), 'head' );

    }

    public function display( $post ) {

        $meta = $this->get_metadata( $post->ID );

        echo self::$template->render([
            'template' => $this->view,
            // TODO: Implement get_headings and mulitpart forms.
            'headings' => $this->form->get_headings(),
            'form' => $this->form->set_values( $meta ),
            'nonce' => wp_nonce_field( "{$this->name}_update_metadata", "{$this->name}_edit_nonce", true, false ),

        ]);

    }

    public function save_box( $id ) {

        if ( ! current_user_can( 'edit_post', $id ) ) {
            return;
        }

        $_POST += [ "{$this->name}_edit_nonce" => '' ];

        if ( ! wp_verify_nonce( $_POST[ "{$this->name}_edit_nonce" ], "{$this->name}_update_metadata" ) ) {
            return;
        }

        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
            return;
        }

        foreach ( $this->form->set_values_from_post() as $ele ) {
            add_post_meta( $id, $ele->slug(), $ele->value(), true)
            or update_post_meta( $id, $ele->slug(), $ele->value() );
        }

    }
}
