<?php
namespace WOR\Metabox;

class Factory {

    protected $metaboxes = [];

    public function __construct(
        $metaboxes = [],
        \WOR\Template\TemplateEnvironmentInterface $template,
        \WOR\Inflector\InflectorInterface $inflector
    ) {
        $this->metaboxes = $metaboxes;
        $this->template = $template;
        $this->inflector = $inflector;
    }

    public function create( $args ) {

        $metabox = new Metabox(
            $args[ 'name' ],
            $args[ 'post_type' ],
            $args[ 'view' ],
            $args[ 'form' ],
            $this->template,
            $this->inflector
        );

        if ( isset( $args[ 'post_name' ] ) ) {
            $metabox->set_post_name( $args[ 'post_name' ] );
        }

        $metabox->add_actions();
        return $metabox;

    }

}
