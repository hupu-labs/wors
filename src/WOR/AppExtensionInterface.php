<?php
namespace WOR;

interface AppExtensionInterface {
    function register_with( \Pimple $app );
}
