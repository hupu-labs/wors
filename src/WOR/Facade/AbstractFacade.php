<?php namespace WOR\Facade;

abstract class AbstractFacade {

    protected static function get_name() {
        throw new \Exception( 'Facade does not implement getName method.' );
    }

    public static function __callStatic($method, $args) {

        $instance = \Wors::get_app()->get( static::get_name() );

        if ( ! method_exists( $instance, $method )) {
            throw new \Exception( get_called_class() . ' does not implement ' . $method . ' method.' );
        }

        return call_user_func_array( [ $instance, $method ], $args );

    }

}
