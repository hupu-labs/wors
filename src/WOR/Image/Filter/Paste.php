<?php
namespace WOR\Image\Filter;

// use \Imagine\Filter\Basic\ApplyMask as ApplyMask;
use \Imagine\Filter\Basic\Paste as ApplyPaste;
use \Imagine\Image\Box;
use \Imagine\Image\Point\Center;
use \Imagine\Image\Point;

class Paste {

    public function __construct( $imagine, $image, $width, $height ) {
        $this->mask = $imagine->open( $image );
        $this->size = [ 'width' => $width, 'height' => $height ];
    }

    public function load() {

        $size = new Box( $this->size[ 'width' ], $this->size[ 'height' ] );
        $point = new Point( 0, 0 );

        return new ApplyPaste( $this->mask->resize( $size ), $point );

    }

}
