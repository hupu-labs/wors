<?php
namespace WOR\Image\Filter;

use \Imagine\Image\Box;
use \Imagine\Image\ImageInterface;
use \Imagine\Filter\Basic\Thumbnail as ImagineThumbnail;
use \Imagine\Filter\Advanced\Grayscale as ImagineGreyscaleFilter;

class Another {

    public function __construct( $size_w, $size_h, $mode ) {
        $this->size_h = $size_h;
        $this->size_w = $size_w;
        $this->mode = $mode;
    }

    public function load() {

        return new ImagineGreyscaleFilter();

        /* $size = new Box( $this->size_w, $this->size_h); */
        /* $mode_const = ( $this->mode === 'outbound' ) */
        /*     ? ImageInterface::THUMBNAIL_OUTBOUND */
        /*     : ImageInterface::THUMBNAIL_INSET; */

        /* return new ImagineThumbnail( $size, $mode_const ); */

    }

}
