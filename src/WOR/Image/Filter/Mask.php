<?php
namespace WOR\Image\Filter;

use \Imagine\Filter\Basic\ApplyMask as ApplyMask;
use \Imagine\Image\Box;

class Mask {

    public function __construct( $imagine, $masking_image, $width, $height ) {
        $this->mask = $imagine->open( $masking_image );
        $this->size = [ 'width' => $width, 'height' => $height ];
    }

    public function load() {

        $size = new Box( $this->size[ 'width' ], $this->size[ 'height' ] );

        return new ApplyMask( $this->mask->resize( $size ) );

    }

}
