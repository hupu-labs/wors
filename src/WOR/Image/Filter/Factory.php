<?php
namespace WOR\Image\Filter;

class Factory {

    protected $elements = [];

    public function __construct( $filters = [] ) {
        $this->filters = $filters;
    }

    public function create( $args = [] ) {

        $factory = $this->filters[ $args[ 'filter' ] ];
        return $factory()->load();

    }
}
