<?php
namespace WOR\Image\Filter;

use Imagine\Image\Point\Center;
use \Imagine\Image\Box;
use \Imagine\Filter\Basic\Crop as ImagineCrop;

class CropFromCenter {

    public function __construct( $size_w, $size_h ) {
        $this->size_h = $size_h;
        $this->size_w = $size_w;
    }

    public function load() {

        $size = new Box( $this->size_w, $this->size_h );
        $center = new Center( $size );

        return new ImagineCrop( $center, $size );

    }

}
