<?php
namespace WOR\Image\Filter;

use \Imagine\Filter\Advanced\Grayscale as ImagineGreyscaleFilter;

class Greyscale {

    public function load() {

        return new ImagineGreyscaleFilter();

    }

}
