<?php
namespace WOR\Imagine\Filter;

use \Imagine\Image\Point;
use \Imagine\Image\Box;
use \Imagine\Filter\Basic\Resize as ImagineResize;

class Resize {

    public static function load( Array $options ) {

        list( $x, $y ) = $options[ 'start' ];
        list( $width, $height ) = $options[ 'size' ];

        $box = new Box( $width, $height );
        $center = new Point( $x, $y );

        return new ImagineResize(
            $box, $center
        );

    }

}
