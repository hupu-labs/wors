<?php
namespace WOR\Image\Filter;

use \Imagine\Image\Point;
use \Imagine\Image\Box;
use \Imagine\Filter\Basic\Crop as ImagineCrop;

class Crop {

    public function __construct( $start_x, $start_y, $size_w, $size_h ) {
        $this->size_h = $size_h;
        $this->size_w = $size_w;
        $this->start_y = $start_y;
        $this->start_x = $start_x;
    }

    public function load() {

        $size = new Box( $this->size_w, $this->size_h);
        $center = new Point( $this->start_x, $this->start_y );

        return new ImagineCrop( $center, $size );

    }

}
