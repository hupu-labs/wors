<?php
namespace WOR\Image\Filter;

use \Imagine\Image\Box;
use \Imagine\Image\ImageInterface;
use \Imagine\Filter\Basic\Thumbnail as ImagineThumbnail;
use \Imagine\Filter\Basic\Resize;
use \Imagine\Filter\Advanced\RelativeResize;
use \Imagine\Filter\Transformation;

class Thumbnail {

    public function __construct( $width, $height, $mode = false, $hard_size = false ) {
        $this->height = $height;
        $this->width = $width;
        $this->mode = $mode;
        $this->hard_size = $hard_size;
    }

    public function load() {

        $mode_const = ( $this->mode === 'outbound' )
            ? ImageInterface::THUMBNAIL_OUTBOUND
            : ImageInterface::THUMBNAIL_INSET;

        $size = new Box( $this->width, $this->height );

        if ( $this->hard_size ) {

            $transformation = new Transformation;

            if ( $this->hard_size === 'width' ) {
                $transformation->add(new RelativeResize( 'widen', $this->width ));
            } elseif ( $this->hard_size === 'height' ) {
                $transformation->add(new RelativeResize( 'heighten', $this->height ));
            }

            $transformation->add(new ImagineThumbnail( $size, $mode_const ));

            return $transformation;

        } else {

            return new ImagineThumbnail( $size, $mode_const );

        }
    }

}
