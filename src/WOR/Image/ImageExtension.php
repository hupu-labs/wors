<?php
namespace WOR\Image;

class ImageExtension {

    public function register_with( \Aura\Di\Container $app ) {
        $app->params[ 'WOR\Image\Filter\Factory' ][ 'filters' ] = [
            'greyscale' => $app->newFactory( 'WOR\Image\Filter\Greyscale' ),
            'logo' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 270,
                'height' => 270,
            ]),
            'user_profile' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 270,
                'height' => 350,
                'mode' => 'outbound'
            ]),
            'fb_widget_thumb' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 300,
                'height' => 200,
                'mode' => 'outbound'
            ]),
            'another' => $app->newFactory( 'WOR\Image\Filter\Another', [
                'width' => 117,
                'height' => 300,
                'mode' => 'outbound'
            ]),
            'full_banner' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 1170,
                'height' => 300,
                'hard_size' => 'width',
                'mode' => 'outbound'
            ]),
            'large_1' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 70,
                'height' => 52,
                'mode' => 'outbound'
            ]),
            'large_2' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 170,
                'height' => 105,
                'mode' => 'outbound'
            ]),
            'large_3' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 270,
                'height' => 157,
                'mode' => 'outbound'
            ]),
            'large_4' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 370,
                'height' => 210,
                'mode' => 'outbound'
            ]),
            'large_5' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 470,
                'height' => 262,
                'mode' => 'outbound'
            ]),
            'large_6' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 570,
                'height' => 315,
                'mode' => 'outbound'
            ]),
            'large_7' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 670,
                'height' => 367,
                'mode' => 'outbound'
            ]),
            'large_8' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 770,
                'height' => 420,
                'mode' => 'outbound'
            ]),
            'large_9' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 870,
                'height' => 472,
                'mode' => 'outbound'
            ]),
            'large_10' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 970,
                'height' => 525,
                'mode' => 'outbound'
            ]),
            'large_11' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 1070,
                'height' => 577,
                'mode' => 'outbound'
            ]),
            'large_12' => $app->newFactory( 'WOR\Image\Filter\Thumbnail', [
                'width' => 1170,
                'height' => 630,
                'mode' => 'outbound'
            ]),
        ];

        $app->set( 'imagine', function () use ( $app ) {
            return new \Imagine\Gd\Imagine;
        });

        $app->set( 'imagine.filter.factory', function () use ( $app ) {
            return $app->newInstance( 'WOR\Image\Filter\Factory' );
        });

        $app->set( 'image.data.factory', function () use ( $app ) {
            return $app->newInstance( 'WOR\Models\Image\Factory', [
                'file_cache' => $app->get( 'cache.file' ),
                'imagine' => $app->get( 'imagine' ),
                'filter_factory' => $app->get( 'imagine.filter.factory' ),
                'uri_factory' => $app->get( 'uri.factory' )
            ]);
        });

        $app->set( 'image.twig_extension', function () use ( $app ) {
            return new ImageTwigExtension( $app->get( 'image.data.factory' ) );
        });


    }
}
