<?php
namespace WOR\Image;

use \Imagine\Image\Box;
use \Imagine\Image\Point;


class ImageTwigExtension extends \Twig_Extension {

    public function __construct( $image_factory ) {
        $this->image_factory = $image_factory;
    }

    public function getFilters() {
        return [
            'image' => new \Twig_Filter_Method( $this, 'image' ),
        ];
    }

    public function image( $src, $filter, $absolute = false ) {
        return $this->image_factory->create( $src )->$filter();
    }


    public function getName() {
        return 'imagine';
    }

}
