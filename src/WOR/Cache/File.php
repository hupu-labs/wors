<?php
namespace WOR\Cache;

class File {
    private $cache_location;
    private $cache_name;
    private $prefix_size = 5;

    public function __construct() {

        global $blog_id;
        $this->cache_location = WP_CONTENT_DIR . '/cache/' . $blog_id . '/';
        $this->url_location = content_url() . '/cache/' . $blog_id . '/';
        $this->mkdir( $this->cache_location );

    }

    public function get_or_create( $file_name, $cache_name, $condition, \Closure $callable ) {
        $file_name = $this->remove_query( $file_name );

        $cached_file_relative = $this->get_cached_file_name( $file_name, $cache_name );
        $cached_url =  $this->url_location . $cached_file_relative;
        $cached_file = $this->cache_location . $cached_file_relative;

        if ( ! $this->check( $cached_file, $condition ) ) {
            $this->mkdir( $cached_file );
            $callable( $cached_file );
        }

        return $cached_url;

    }

    private function remove_query( $value ) {

        if ( preg_match( '/^([^\?]+)/', $value, $matches )) {
            return $matches[1];
        } else {
            return $value;
        }

    }

    private function check( $cached_file, $condition ) {

        if ( ! file_exists( $cached_file ) ) {
            return false;
        }

        $age = time() - filectime( $cached_file );
        return ( $age < $condition );

    }

    private function mkdir( $cached_file ) {

        $file = substr( strrchr( $cached_file, "/" ), 1 ) ;
        $dir = str_replace( $file, '', $cached_file) ;

        if ( ! is_dir( $dir) ) {
            if ( ! mkdir( $dir, 0750, true ) ) {
                throw new \Exception( 'Cannot create folder: ' . $dir);
            }
        }

    }

    private function get_cached_file_name( $full_file_name, $cache_name ) {

        // Getting the length of the filename before the extension
        $basename = basename( $full_file_name );
        $file_name_parts = explode( '.', $basename );
        $file_name = $file_name_parts[ 0 ];
        $file_name_length = strlen( $file_name );

        $cache_path = [];
        $effective_prefix_length = min( $file_name_length, $this->prefix_size );
        for ( $i = 0; $i < $effective_prefix_length; $i++ ) {
            $cache_path[] = $file_name[ $i ];
        }

        return (
            $cache_name .
            '/' .
            implode( '/', $cache_path ) .
            '/' .
            $basename
        );

    }

}
