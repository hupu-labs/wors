<?php
namespace WOR\Cache;

class Transient {

    static function create( $name, \Closure $callable ) {

        $cache = get_transient( $name );

        if ( empty( $cache ) ) {

            $response = $callable();
            set_transient( $name, $response, 60*60 );
            return $response;

        } else {

            return $cache;

        }
    }

}
