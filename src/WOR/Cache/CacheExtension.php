<?php
namespace WOR\Cache;

class CacheExtension {

    public function register_with( \Aura\Di\Container $app ) {

        $app->set( 'cache.file', function () {
            return new File;
        });

        $app->set( 'cache.transient', function () {
            return new Transient;
        });

    }

}
