<?php
namespace WOR\Inflector;

class AppExtension  {

    public function register_with( \Aura\Di\Container $app ) {

        $app->set( 'inflector', function () {
            $inflector = Inflector::get();
            return $inflector;
    });

        $app->set( 'inflector.twig_extension', function () use ( $app ) {
        return new InflectorTwigExtension( $app->get( 'inflector' ) );
        });

    }
}
