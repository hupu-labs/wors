<?php
namespace WOR\Inflector;

class InflectorTwigExtension extends \Twig_Extension {

    public function __construct( Inflector $inflector ) {
        $this->inflector = $inflector;
    }

    public function getFilters() {
        return [
            'inflect' => new \Twig_Filter_Method( $this, 'inflect' ),
        ];
    }

    public function inflect( $text, $filter ) {
        return $this->inflector->$filter( $text );
    }

    public function getName() {
        return 'inflect';
    }

}
