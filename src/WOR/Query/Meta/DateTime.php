<?php
namespace WOR\Query\Meta;
use \Functional as F;

class DateTime extends Base {

    protected function get_type() {
        return 'DATE';
    }

    protected function get_value() {

        if ( preg_match( '@(?:\d+)(?:/\d+)?(?:/\d+)?/?@', $this->value ) == 0 ) {

            return date( 'Y-m-d', strtotime( $this->value ) );

        } else {

            $date = F\filter( explode( '/', $this->value ), 'not_empty' );

            switch ( count( $date ) ) {

            case 1:

                $this->set_operator( 'between' );

                return [
                    date( $date[0] . '01' . '01' ),
                    date( $date[0] . '12' . 't' )
                ];

            case 2:

                $this->set_operator( 'between' );

                return [
                    date( $date[0] . $date[1] . '01' ),
                    date( $date[0] . $date[1] . 't' )
                ];

            case 3:

                return date( 'Y-m-d', strtotime( $this->value ) );

            default:
                break;

            }

        }

    }

    protected function get_operator() {

        switch ( $this->operator ) {

        case 'after':
            return '>=';

        case 'before':
            return '<=';

        default:
            return strtoupper( $this->operator );

        }

    }

}
