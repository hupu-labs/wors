<?php
namespace WOR\Query\Meta;

abstract class Base {

    const QUERY_TYPE = 'meta_query';
    protected $field;
    protected $operator;
    protected $value;
    abstract protected function get_type();

    public function __construct( $field ) {
        $this->field = $field;
    }

    public function is( $operator, $value ) {
        $this->set_operator( $operator );
        $this->set_value( $value );
        return $this;
    }

    protected function set_operator( $operator ) {
        $this->operator = $operator;
    }

    protected function set_value( $value ) {
        $this->value = $value;
    }

    public function get_query() {

        return [
            'key' => $this->get_field(),
            'value' => $this->get_value(),
            'compare' => $this->get_operator(),
            'type' => $this->get_type(),
        ];

    }

    protected function get_field() {
        return $this->field;
    }

    protected function get_value() {
        return $this->value;
    }

    protected function get_operator() {
        return strtoupper( $this->operator );
    }

}
