<?php
namespace WOR\Query\Date;

class Base {
    const QUERY_TYPE = 'date_query';

    public function __construct() {

    }

    public function is( $method, $first_date, $second_date = null ) {
        return $this;
    }

    public function get_query() {
        return [
            'year'  => 2014,
            'month' => 12,
            'day'   => 24,
        ];
    }
}
