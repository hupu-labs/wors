<?php
namespace WOR\Mail;

class MailExtension  {

    public function register_with( \Aura\Di\Container $app ) {

        $app->set( 'mail.mailer', function () use ( $app ) {
            return new Mailer( $app->params[ 'domain' ] );
        });

    }
}
