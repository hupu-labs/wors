<?php
namespace WOR\Mail;

class Mailer {

    public function __construct( $domain ) {
        $this->domain = (String) $domain;
    }

    public function send_mail( $to, $from_address, $from_name, $subject, $body ) {

        $headers = [
            'MIME-Version: 1.0',
            'Content-type: text/plain; charset=iso-8859-1',
            'From: ' . get_bloginfo( 'name' ) . ' no-reply <no-reply@' . $this->domain . '>',
            'Reply-To: ' . $from_name . ' <' . $from_address . '>',
            'Subject: {' . $subject . '}',
            'X-Mailer: PHP/' . phpversion()
        ];

        return mail( $to, $subject, $body, implode( "\r\n", $headers ));

    }

}
