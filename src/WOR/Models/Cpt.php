<?php
namespace WOR\Models;

abstract class Cpt extends ModelAbstract {
    private static $inflector;
    public $connection;
    protected $cpt;
    private $names;

    public function __construct(
        $wp_post_factory,
        $wp_query_factory,
        \WOR\Inflector\InflectorInterface $inflector,
        $connections = null
    ) {

        parent::__construct( $wp_post_factory, $wp_query_factory, $inflector, $connections );
        self::$inflector = $inflector;

        $this->init_cpt();
        $this->get_names( $this->query[ 'post_type' ] );

        $this->taxonomies = isset( $this->cpt[ 'taxonomies' ] ) ? $this->cpt[ 'taxonomies' ]: false;

        $this->register();
        $this->set_taxonamies();

    }

    abstract function init_cpt();

    private function connections() {
        if ( isset( $this->cpt[ 'connections' ] ) ) {
            foreach ( $this->cpt[ 'connections' ] as $connection => $args ) {
                $this->connection->set_connection( $connection );
            }
        }
    }

    private function get_names( $class ) {

        $inf = self::$inflector;
        $title = isset( $this->cpt[ 'name' ] ) ?
            $this->cpt[ 'name' ] :
            self::$inflector->titleize( $class );
        $this->cpt[ 'title' ] = $title;
        $this->cpt[ 'name' ] = $title;
        $slug = sanitize_title( $title );


        $this->cpt[ 'plural_name' ] = isset( $this->cpt[ 'plural_name' ] ) ?
            $this->cpt[ 'plural_name' ] :
            $inf->titleize( $inf->pluralize( $this->cpt[ 'name' ] ) );

        $this->cpt[ 'slug' ] = isset( $this->cpt[ 'slug' ] ) ?
            $this->cpt[ 'slug' ] :
            $slug;

        $this->query[ 'post_type' ] = $this->cpt[ 'slug' ];
        $this->cpt[ 'post_type' ] = $this->cpt[ 'slug' ];

    }

    public function register() {
        $cpt = $this->cpt;
        $inf = self::$inflector;
        $labels = array(
            'name' => $cpt[ 'plural_name' ],
            'singular_name' => $cpt[ 'name' ],
            'add_new' => 'Add ' . $cpt[ 'name' ],
            'all_items' => 'All ' . $cpt[ 'plural_name' ],
            'add_new_item' => 'Add New ' . $cpt[ 'name' ],
            'edit_item' => 'Edit ' . $cpt[ 'name' ],
            'new_item' => 'New ' . $cpt[ 'name' ],
            'view_item' => 'View ' . $cpt[ 'name' ],
            'search_items' => 'Search ' . $cpt[ 'name' ],
            'not_found' => 'No ' . $cpt[ 'plural_name' ] . ' Found',
            'not_found_in_trash' => 'No ' . $cpt[ 'plural_name' ] . ' Found in Trash',
            'menu_name' => $cpt[ 'plural_name' ]
        );

        $args = [
            'labels' => $labels,
            'public' => true,
            'menu_icon' => 'dashicons-' . (
                isset( $cpt[ 'menu_icon' ] )
                ? $cpt[ 'menu_icon' ]
                : 'admin-post'
            ),
            'menu_position' => (
                isset( $cpt[ 'menu_position' ] )
                ? $cpt[ 'menu_position' ]
                : 5
            ),
            'supports' => (
                isset( $cpt[ 'supports' ] )
                ? $cpt[ 'supports' ]
                : [ 'title', 'editor', 'thumbnail', 'tags' ]
            ),
            'has_archive' => true,
            'rewrite' => [
                'slug' => $cpt[ 'slug' ]
            ]
        ];

        register_post_type( $cpt[ 'slug' ], $args );

    }

}
