<?php
namespace WOR\Models;
use \Functional as F;

class WpPostFactory {

    public function __construct( $meta_data_factory, $image_factory, $inflector ) {
        $this->meta_data_factory = $meta_data_factory;
        $this->image_factory = $image_factory;
        $this->inflector = $inflector;
    }

    public function create( $post, $parent = false, $connected_relationships = null, $connections = null ) {

        $wor_post = new WpPost(
            $post,
            $post->ID,
            $parent,
            $this->meta_data_factory->create( $post->ID ),
            $this->image_factory->create( get_featured( $post->ID ) ),
            $connections,
            $this->inflector
        );

        if ( $connected_relationships ) {

            foreach ( $connected_relationships as $relationship ) {

                $post_type = $relationship[ 'post_type' ];
                $post->$post_type = F\map( $post->$post_type, function ( $e ) use ( $wor_post ) {
                    return $this->create( $e, $wor_post );
                });

            }

        }

        return $wor_post;

    }

}
