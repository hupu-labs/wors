<?php
namespace WOR\Models;


class Admin {
    private static $_instance;
    public function __construct( ) {
        $this->add_actions();
    }
   protected function add_actions() {
       \WOR\Dbg\Debug::pp('admin add_actions', __FILE__);
       add_action( 'admin_init', [ $this, 'add_theme_option' ],99 );
        add_action( 'admin_menu', [ $this, 'add_admin_menu' ] );
    }

    public function add_theme_option() {
        // Registers the settings fields and callback
        \WOR\Dbg\Debug::pp('admin add_theme_option', __FILE__);
        register_setting( 'wor_theme_options', 'wor_options', 'wor_validate' );
    }

    function add_admin_menu() {
        \WOR\Dbg\Debug::pp('add_admin_menu', __FILE__);
        add_menu_page(
            'wor_theme_options',
            'WOR Options',
            'manage_options',
            'wor_theme_options',
            function () {
                echo 'that';
            }//[ $this,'admin_page' ]
        );
        add_submenu_page(
            'wor_theme_options',
            '1st Real Submenu',
            '1st Real Submenu',
            'manage_options',
            '1st_real_submenu_slug',
            function () {
                echo 'shit';
            }
        );
    }
}
