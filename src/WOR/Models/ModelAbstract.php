<?php
namespace WOR\Models;
use \MonadPHP as M;
use \Functional as F;

abstract class ModelAbstract {

    use WorsQuery;
    use Posts2Posts;

    protected $pages = null;
    // protected $connections = null;

    // private $is_where_connected = false;
    private static $inflector;
    // protected $query;
    protected $post_type_class;
    protected $data;
    protected $query_obj;
    protected $posts;

    public function __construct(
        $wp_post_factory,
        $wor_query_factory,
        \WOR\Inflector\InflectorInterface $inflector,
        $connections = null
    ) {
        self::$inflector = $inflector;

        $this->wp_post_factory = $wp_post_factory;
        $this->wor_query_factory = $wor_query_factory;

        $this->set_post_type();
        $this->ensure_required_pages();
        $this->query[ 'connected_relationships' ] = null;

        $this->connections = $connections;
    }

    private function flush() {

        if ( array_key_exists( 's', $this->query) ) {
            $this->query = null;
            return;
        }

        $post_type = $this->query[ 'post_type' ];
        $this->query = null;
        $this->query['connected_relationships'] = null;
        $this->query[ 'post_type' ] = $post_type;
        $this->frozen = false;
    }

    public function where_connected( $type, $ids ) {
        $this->is_where_connected = true;
        $this->query[ 'where_connected' ][$type] = $ids;
        return $this;
    }

    public function including_connected( $connnected_post_type, $tax_ids = null ) {

        if ( is_array( $connnected_post_type ) ) {

            $model = $connnected_post_type[ 'model' ];
            $with_connected = $connnected_post_type[ 'including' ];

        } else {

            $model = $connnected_post_type;
            $with_connected = [];

        }

        $this->query[ 'connected_relationships' ][] = [
            'post_type' => self::$inflector->singularize( $model ),
            'including' => F\map( $with_connected, function ($e) {
                return [
                    'post_type' => self::$inflector->singularize($e),
                    'taxonomy_ids' => null,
                ];
            }),
            'taxonomy_ids' => $tax_ids,
        ];

        return $this;
    }

    public function __call( $name, $args ) {

        if ( strpos( $name, 'with_' ) === 0 ) {
            $this->with_taxonomy( substr( $name, 5 ), $args);
        } elseif ( strpos( $name, 'get_' ) === 0 ) {
            $this->get_related( substr( $name, 4 ) );
        } else {
            throw new \Exception('no magic method');
        }

        return $this;
    }

    public function orderby_date_meta( $field ) {
        $this->orderby_meta_num( $field );
        return $this;
    }

    public function where( $sub_query ) {
        $this->query[ $sub_query::QUERY_TYPE ][] = $sub_query->get_query();
        return $this;
    }

    public function preview() {
        unset( $this->query[ 'posts_per_page' ] );
        $this->query[ 'preview' ] = 'true';
        return $this;
    }

    public function with_id( $id ) {
        $this->query[ 'p' ] = $id;
        return $this;
    }

    public function with_post_type( $post_type ) {
        $this->query[ 'post_type' ] = $post_type;
        return $this;
    }

    public function order_by( $order, $direction ) {
        $this->query[ 'orderby' ] = $order;
        $this->query[ 'order' ] = $direction;
        return $this;
    }

    public function with_category( $category ) {
        $this->with_categories( [ $category ] );
        return $this;
    }

    public function with_categories( $categories ) {
        $this->with_taxonomy( 'category', $categories );
        return $this;
    }

    public function with_taxonomy( $taxonomy, $terms ) {

        if ( ! empty( $terms ) ) {
            $this->query[ 'tax_query' ][] = [
                'taxonomy' => $taxonomy,
                'field' => 'slug',
                'terms' => $terms,
            ];
        }

        return $this;
    }

    public function with_meta_key( $key ) {
        $this->query[ 'meta_key' ] = $key;
        return $this;
    }

    public function with_meta( $meta ) {
        $this->query[ 'meta_key' ] = key( $meta );
        $this->query[ 'meta_value' ] = $meta[ key( $meta ) ];
        return $this;
    }

    public function with_name( $name ) {
        if ( $this->query[ 'post_type' ] == 'page' ) {
            $this->query[ 'pagename' ] = $name;
        } else {
            $this->query[ 'name' ] = $name;
        }
        return $this;
    }


    public function with_search( $search_terms ) {
        $this->query = [];
        $this->query[ 's' ] = $search_terms;
        return $this;
    }


    public function get_wp_query() {

        if ( $this->frozen ) {
            return $this->query_obj;
        } else {
            return $this->set_query();
        }

    }

    function setup_postdata() {
        $GLOBALS['post'] = $this->query_obj;
    }

    private function set_query() {

        $this->frozen = true;
        $this->wp_query_obj = new \WP_Query( $this->query );

        if ( ! isset( $this->query[ 's' ] ) ) {
            global $wp_query;
            $wp_query = $this->wp_query_obj;
            return $wp_query;
        } else {
            return $this->wp_query_obj;
        }

    }

    private function ensure_required_pages() {

        if ( $this->pages ) {

            foreach ( $this->pages as $page ) {

                if ( ! get_page_by_title( $page ) ) {

                    wp_insert_post([
                        'post_title' => $page,
                        'post_type' => 'page',
                        'post_status' => 'publish',
                    ]);

                }

            }

        }
    }

    private function set_post_type() {
        $this->post_type_class = get_called_class();
        $class_array = explode( '\\', $this->post_type_class );
        $class = end( $class_array );
        $this->query[ 'post_type' ] = self::$inflector->underscore( $class );
    }


    private function create_from_posts(
        $out,
        $relationship_name = null,
        $relationships = null
    ) {

        if (! $relationships ) {
            $relationships = is( $this->query[ 'connected_relationships' ], [] );
        }

        $posts = $this->wor_query_factory->create( new \WP_Query( $this->query ) );

        // TODO this needs to be tested
        $this->setup_relationships(
            $relationships,
            $relationship_name,
            $out
        );

        foreach ( $out as $post ) {

            $posts->add( $this->wp_post_factory->create(
                $post,
                false,
                $this->query[ 'connected_relationships' ],
                $this->connections,
                'create_from_posts'
            ));

        }

        return $posts;

    }

    private function get_posts() {

        // if ( $this->is_where_connected ) {
        //     $this->get_related_two();
        // }

        // TODO: defualt instance vars, where appropriate instance vars need to be objects
        // with defautl args themselves.
        $this->setup_relationships(
            is( $this->query[ 'connected_relationships' ], [] )
        );

        $posts = $this->wor_query_factory->create( $this->wp_query_obj );

        if ( $this->wp_query_obj->have_posts() ) {

            global $post;

            while ( $this->wp_query_obj->have_posts() ) {

                $this->wp_query_obj->the_post();

                $posts->add( $this->wp_post_factory->create(
                    $post,
                    false,
                    $this->query[ 'connected_relationships' ],
                    $this->connections,
                    'get_posts'
                ));

            }

            if ( $this->is_where_connected ) {
                remove_all_actions( 'posts_where' );
            }

            return $posts;

        } else {

            return false;

        }

    }

    public function loop_as_name_id_array( $number ) {

        $array = [];

        $posts = $this->loop( $number );
        if ( $posts ) {
            foreach ( $posts as $name => $post ) {
                $array[ $post->id() ] = $post->title();
            }
        return $array;
        } else {
            return false;
        }

    }

    public function loop_one() {

        if ( $loop = $this->loop( 1 ) ) {
            return $loop[ 0 ];
        } else {
            return false;
        }

    }

    public function loop( $number ) {
        $this->query[ 'posts_per_page' ] = $number;
        $this->set_query();
        $posts = $this->get_posts();
        $this->flush();
        return $posts;
    }

    protected function set_taxonamies() {

        if ( $this->taxonomies ) {

            foreach ( $this->taxonomies as $tax ) {

                $inf = self::$inflector;
                $slug = isset( $tax[ 'slug' ] ) ? $tax[ 'slug' ] : sanitize_title( $tax[ 'name' ] );
                $name = $tax[ 'name' ];
                $plural_name = $inf->pluralize( $name );

                if ( taxonomy_exists( $slug ) ) {

                    register_taxonomy_for_object_type( $slug, $this->query[ 'post_type' ] );

                } else {

                    register_taxonomy( $slug, $this->query[ 'post_type' ], [

                        'hierarchical' => true,

                        'labels' => [
                            'name' => $plural_name,
                            'singular_name' => $name,
                            'search_items' => 'Search ' . $name,
                            'all_items' => 'All ' . $plural_name,
                            'parent_item' => 'Parent ' . $name,
                            'parent_item_colon' => 'Parent :' . $name,
                            'edit_item' => 'Edit ' . $name,
                            'update_item' => 'Update ' . $name,
                            'add_new_item' => 'Add New ' . $name,
                            'new_item_name' => 'New ' . $name,
                            'menu_name' => __( $plural_name ),
                        ],

                        'rewrite' => [
                            'slug' => $slug,
                            'with_front' => false,
                            'hierarchical' => true,
                        ],

                    ]);

                }

            }

        }

    }

}
