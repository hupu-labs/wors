<?php
namespace WOR\Models\Gallery;

class Factory {

    public function __construct( $image_factory ) {
        $this->image_factory = $image_factory;
    }

    public function create( $gallery_json ) {
        $gallery_array = json_decode( $gallery_json );
        return new Gallery(
            $gallery_array,
            $this->image_factory
        );

    }

}
