<?php
namespace WOR\Models\Gallery;
use \Functional as F;

class Gallery implements \IteratorAggregate, \ArrayAccess {

    public function __construct( Array $gallery_array, $image_factory ) {
        $this->images = F\map( $gallery_array, function ( $item ) use ( $image_factory ) {
            $image = $image_factory->create( $item->url );
            $image->set_caption( $item->caption );
            return $image;
        });
    }


    public function getIterator() {
        return new \ArrayIterator( $this->images );
    }

    public function offsetSet( $id, $value ) {
        $this->images[ $id ] = $value;
    }

    public function offsetGet( $id ) {
        if ( ! array_key_exists( $id, $this->images ) ) {
            throw new InvalidArgumentException( sprintf( 'Form element, "%s", is not defined.', $id ) );
        }

        return $this->images[ $id ];
    }

    public function offsetExists( $id ) {
        return array_key_exists( $id, $this->images );
    }

    public function offsetUnset( $id ) {
        unset( $this->images[ $id ] );
    }
}
