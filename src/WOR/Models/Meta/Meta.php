<?php
namespace WOR\Models\Meta;
use \Functional as F;
use MonadPHP as M;

class Meta {
    var $metadata;

    public function __construct( $post_id ) {
        $this->metadata = $this->get_metadata( $post_id );
    }

    public function __call( $name, $args ) {

        if ( isset( $this->metadata->$name ) && $this->metadata->$name ) {
            return $this->metadata->$name;
        } else {
            return false;
        }

    }

    public function __get( $name ) {
        if ( isset( $this->metadata->$name ) ) {
            if ( is_serialized( $this->metadata->$name ) ) {
                return unserialize( $this->metadata->$name );
            } else {
                return $this->metadata->$name;
            }
        } else {
            return false;
        }
    }

    public function __isset( $name ) {
        return isset( $this->metadata->$name );
    }

    private function get_metadata( $post_id ) {
        return (Object) F\map( get_post_meta( $post_id ), 'head' );
    }

}
