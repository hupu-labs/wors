<?php
namespace WOR\Models\Meta;

class Factory {

    public function create( $id ) {
        return new Meta( $id );
    }

}
