<?php
namespace WOR\Models;

class Term {
    private $tags;

    public function __construct() {
        $this->tags = $this->set_tags();
    }

    public function set_tags() {
        return array_map(
            function ( $e ) {
                return (Object) [
                    'text' => $e->name,
                    'weight' => $e->count,
                    'link' => $e->slug,
                ];
            },
            get_tags()
        );
    }

    public function get_tags() {
        return $this->tags;
    }

    public function get_json_tags() {
        return json_encode( $this->get_tags() );
    }

    public function get_ordered_tags_multi_dim() {
        return array_reduce( $this->tags, function ( $result, $e ) {
            $result[ $e->weight ][] = $e->text; return $result;
        });
    }

    public function get_ordered_tags() {
        $tags = $this->tags;
        usort( $tags, function ($a, $b) {
            return $a->weight <= $b->weight;
        });
        return $tags;
    }
}
