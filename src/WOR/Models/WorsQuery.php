<?php
namespace WOR\Models;

trait WorsQuery {

    protected $query = [];

    public function orderby_meta_num( $field ) {
        $this->query[ 'orderby' ] = 'meta_value_num';
        $this->query[ 'meta_key' ] = $field;
        return $this;
    }

    public function sticky() {
        $this->query[ 'post__in' ] = get_option( 'sticky_posts' );
        return $this;
    }

    public function with_tag( $tag ) {
        $this->query[ 'tag' ] = $tag;
        return $this;
    }

    public function page( $value ) {
        $this->query[ 'paged' ] = $value;
        return $this;
    }

    public function paged( $value ) {
        $this->query[ 'paged' ] = $value ?: 1;
        return $this;
    }

    public function with_meta_value( $value ) {
        $this->query[ 'meta_value' ] = $value;
        return $this;
    }

}
