<?php
namespace WOR\Models;
use \MonadPHP as M;

class WpPost {

    static $inflector;
    var $object;
    var $id;
    var $taxonomies;

    public function __construct(
        $object,
        $id,
        $parent,
        $meta,
        $featured,
        $connections,
        $inflector
    ) {
        $this->object = $object;
        $this->id = $id;
        $this->meta = $meta;
        $this->featured = $featured;
        $this->parent = $parent;

        $this->connections = $connections;

        self::$inflector = $inflector;

        foreach ( get_object_taxonomies( $this->object ) as $tax ) {
            $this->taxonomies[ $tax ] = true;
        }

    }

    public function id() {
        return (string) $this->id;
    }


    public function p2p_id() {
        return $this->object->p2p_id;
    }

    public function get_connected( $connected_post_type ) {

        $singular_post_type = self::$inflector->singularize( $connected_post_type );
        $connected_posts = $this->object->$singular_post_type;

        $connection_names = [];

        if ( $this->connections ) {
            foreach ( $this->connections as $connection ) {
                if ( $connection->keys() ) {
                    $connection_names = array_merge( $connection->keys(), $connection_names );
                }
            }
        }

        if ( $connected_posts ) {
            foreach ( $connected_posts as $post ) {
                foreach ( $connection_names as $connection_name ) {
                    $post->$connection_name = p2p_get_meta( $post->p2p_id(), $connection_name, true );
                }
            }
        }

        return $connected_posts;

    }

    public function meta() {
        return $this->meta;
    }

    public function featured() {
        return $this->featured;
    }

    public function get_terms_of_type( $tax ) {
        if ( $terms = get_the_terms( $this->id, $tax ) ) {
            return $terms;
        } else {
            return false;
        }

    }

    public function __call( $name, $args ) {
        if ( $terms = get_the_terms( $this->id, $name ) ) {
            return $terms;
        } else {
            return false;
        }
    }

    public function title() {
        return get_the_title( $this->object );
    }

    public function raw_content() {
        return $this->object->post_content;
    }

    public function date() {
        return $this->get_the_date();
    }

    public function content() {
        return apply_filters('the_content', $this->raw_content() );
    }

    public function tags() {
        return get_the_terms( $this->id, 'post_tag' );
    }

    public function categories() {
        return get_post_categories( $this->id );
    }

    public function excerpt() {

        if ( post_password_required( $this->object ) ) {
            return __( 'There is no excerpt because this is a protected post.' );
        }

        return apply_filters( 'get_the_excerpt',  $this->get_the_excerpt() );
    }

    private function get_the_excerpt() {
        $the_excerpt = $this->object->post_content; //Gets post_content to be used as a basis for the excerpt
        $excerpt_length = 55; //Sets excerpt length by word count
        $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
        $words = explode(' ', $the_excerpt, $excerpt_length + 1);

        if(count($words) > $excerpt_length) {
            array_pop($words);
            array_push($words, '…');
            $the_excerpt = implode(' ', $words);
        }

        $the_excerpt = '<p>' . $the_excerpt . '</p>';

        return $the_excerpt;

    }


    public function slug() {
        return $this->object->post_name;
    }

    public function url() {

        // if ($this->parent) {
        //     $parent = $this->parent->url();
        // } else {
        //     $parent = '';
        // }

        $url = (
            '/' .
            $this->object->post_type .
            '/' .
            $this->object->post_name
        );

        return $url;
        // return $parent . $url;
    }

    private function get_the_date( $d = '' ) {
        $the_date = '';

        if ( '' == $d )
            $the_date .= mysql2date(get_option('date_format'), $this->object->post_date);
        else
            $the_date .= mysql2date($d, $this->object->post_date);

        return apply_filters('get_the_date', $the_date, $d);
    }
}
