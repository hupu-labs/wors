<?php
namespace WOR\Models;

class WpQueryFactory {

    public function __construct( $inflector ) {
        $this->inflector = $inflector;
    }

    public function create( $query ) {

        return new WpQuery(
            $this->inflector->pluralize( $query->query[ 'post_type' ] ),
            $query
        );
    }

}
