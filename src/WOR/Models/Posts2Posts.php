<?php
namespace WOR\Models;

trait Posts2Posts {

    protected $connections = null;
    private $is_where_connected = false;

    private function get_p2p( $type_1, $type_2 ) {
        return p2p_type( $type_1 . '_to_' . $type_2 )
           ?:  p2p_type( $type_2 . '_to_' . $type_1 );
    }

    private function setup_relationship( $relationship, $type, $out = null ) {

        $relationship_post_type = $relationship[ 'post_type' ];

        if ( $relationship[ 'taxonomy_ids' ] ) {
            $qv = $relationship[ 'taxonomy_ids' ];
        } else {
            $qv = [];
        }

        if ( $type ) {

            if ( $out ) {

                $type->each_connected( $out, $qv, $relationship_post_type );

            } else {

                $type->each_connected( $this->wp_query_obj, $qv, $relationship_post_type );

                if ( ! empty( $relationship[ 'including' ] ) ) {

                    foreach ( $this->wp_query_obj->posts as $post ) {

                        $this->create_from_posts(
                            $post->$relationship_post_type,
                            $relationship_post_type,
                            $relationship[ 'including' ]
                        );

                    }
                }
            }
        }
    }

    private function setup_relationships( $relationships, $post_type = null, $out = null ) {
        foreach ( $relationships as $relation ) {
            $relation_end = $relation[ 'post_type' ];
            if ( ! $post_type ) {
                $post_type = $this->query[ 'post_type' ];
            }
            $type = $this->get_p2p( $relation_end, $post_type );
            $this->setup_relationship( $relation, $type, $out );
        }
    }

    private function get_related() {

        global $wpdb;

        $base = $wpdb->prepare((
            "SELECT *
             FROM $wpdb->posts AS p
             WHERE p.post_type = '%s' \n"
        ), $this->query[ 'post_type' ]);

        $out = [];

        foreach ( $this->query['where_connected'] as $type => $ids ) {

            $where_out = [];

            if ( p2p_type( $type . '_to_' . $this->query['post_type'] ) ) {
                $direction = true;
            } elseif ( p2p_type( $this->query['post_type'] . '_to_' . $type ) ) {
                $direction = false;
            }

            if ( $ids ) {

                foreach ( $ids as $id ) {

                    $where_out[] = $wpdb->prepare((
                        "$wpdb->p2p.p2p_" .
                        ( $direction ? 'from' : 'to' ) .
                        " = %d \n"
                    ), $id);

                }

                $where_out_string = implode( ' OR ', $where_out );

            }

            if ( ! empty( $where_out ) ){
                $out[] = (
                    "(

                        select count(*) from $wpdb->p2p
                        inner join $wpdb->posts on
                        (
                            $wpdb->posts.ID = $wpdb->p2p.p2p_from or
                            $wpdb->posts.ID = $wpdb->p2p.p2p_to
                        )
                        where $wpdb->posts.ID = $wpdb->p2p.p2p_" .
                        ( $direction ? 'to' : 'from' ) .
                        " and ( $where_out_string )

                    ) >= 1"
                );

            }

        }

        if ( ! empty( $out ) ) {
            $final_out = $base . ' AND (' . implode( ' AND ', $out ) . ')';
        } else {
            $final_out = $base;
        }

        $final_out .=  ' GROUP BY p.ID LIMIT 10';

        return $this->create_from_posts( $wpdb->get_results( $final_out ) );

    }

}
