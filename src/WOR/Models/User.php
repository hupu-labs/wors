<?php
namespace WOR\Models;

class User {

    private $query;

    public function __construct() {


    }

    public function get_user_data( $name ) {
        $user = $this->get_user_by_name( $name );
        $bio = get_the_author_meta( 'description', $user->data->ID );
        return [ 'bio' => $bio, 'user' => $user ];
    }
    public function get_user_by_name( $name ) {
        return get_user_by( 'slug', $name );
    }

    public function with_name( $name ) {
        $this->query[ 'meta_key' ] = 'slug';
        $this->query[ 'meta_value' ] = $name;
        return $this;
    }

    public function loop() {
        $user_query = new \WP_User_Query( $this->query );
        /* pp( $user_query ); */
        return $user_query->results;
    }
}
