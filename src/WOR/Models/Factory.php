<?php
namespace WOR\Models;

class Factory {

    protected $models = [];

    public function __construct(
        $models = [],
        $wp_post_factory,
        $wp_query_factory,
        $inflector
    ) {
        $this->wp_post_factory = $wp_post_factory;
        $this->wp_query_factory = $wp_query_factory;
        $this->models = $models;
        $this->inflector = $inflector;
    }

    public function get_models() {

        $out = [];

        foreach ( $this->models as $key => $model ) {
            $out[] = $key;
        }

        return $out;
    }

    public function create( $model_name ) {

        $factory = $this->models[ $model_name ];

        return $factory(
            $this->wp_post_factory,
            $this->wp_query_factory,
            $this->inflector
        );

    }

}
