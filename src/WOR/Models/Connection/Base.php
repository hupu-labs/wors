<?php
namespace WOR\Models\Connection;

class Base {

    protected $keys = null;

    public function __construct( $from, $to, $args = null ) {
        $this->from = $from;
        $this->to = $to;
        $this->args = $args;

        if ( isset( $args[ 'fields' ] ) ) {
            $this->keys = array_keys( $args['fields'] );
        }

        $this->add_actions();
    }

    public function keys() {
        return $this->keys;
    }

    public function register_connection() {

        $args = [
            'name' => $this->from . '_to_' . $this->to,
            'from' => $this->from,
            'to' => $this->to,
            'sortable' => 'any',
        ];

        if ( $this->args ) {

            $args = array_merge( $this->args, $args );

        }

        p2p_register_connection_type( $args );

    }

    private function add_actions() {
        add_action( 'p2p_init', [ $this, 'register_connection' ]);
    }

}
