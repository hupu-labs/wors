<?php

namespace WOR\Models\Image;

class Factory {

    public function __construct( $file_cache, $imagine, $filter_factory, $uri_factory ) {
        $this->file_cache = $file_cache;
        $this->imagine = $imagine;
        $this->filter_factory = $filter_factory;
        $this->uri_factory = $uri_factory;
    }

    public function create( $src ) {

        return new Image(
            $src,
            $this->file_cache,
            $this->imagine,
            $this->filter_factory,
            $this->uri_factory
        );

    }

}
