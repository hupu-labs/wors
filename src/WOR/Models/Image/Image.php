<?php

namespace WOR\Models\Image;
use \WOR\Cache\File as FileCache;

class Image {

    public function __construct( $src, FileCache $file_cache, $imagine, $filter_factory, $uri ) {
        $this->src = $src;
        $this->file_cache = $file_cache;
        $this->imagine = $imagine;
        $this->filter_factory = $filter_factory;
        $this->uri = $uri;
    }

    public function set_caption( $caption ) {
        $this->caption = $caption;
    }

    private function get_func( $src, $filter ) {
        return function ( $cache_path ) use ( $src, $filter ) {

            if ( ! empty( $src ) ) {

                $filter = $this->filter_factory->create([ 'filter' => $filter ]);
                $filter->apply( $this->imagine->open( $src ) )
                       ->save( $cache_path, [ 'jpeg_quality' => 100 ]);
            }

        };
    }

    public function filter_image( $filter, $absolute = false ) {

        if ( strstr( $this->src, 'safe_image.php' ) ) {
            $this->src = $this->uri->newInstance( $this->src )->query[ 'url' ];
        }

        if ( $this->src ) {

            return $this->file_cache->get_or_create(
                $filter . '_' . basename( $this->src ),
                $filter,
                // 1,
                WEEK_IN_SECONDS * 4,
                $this->get_func( $this->src, $filter )
            );

        }
    }

    public function __call( $filter, $args ) {
        $this->src = $this->filter_image( $filter );
        return $this;
    }

    public function url() {
        return $this->src;
    }

    public function __toString() {

        if ( $this->src ) {
            $metadata = getimagesize($this->src);
            return (
                '<img class="featured wp-post-image" width="' .
                $metadata[ 0 ] .
                '" height="' .
                $metadata[ 1 ] .
                '" alt="' .

                '" src="' .
                $this->src .
                '">'
            );
        } else {
            return '';
        }

    }

}
