<?php

namespace WOR\Models\Image;

class ThemeSupport {

    public function __construct(
        $base,
        $gutter
    ) {

        $this->base = $base;
        $this->gutter = $gutter;

        $this->add_theme_supports();

    }

    private function add_theme_supports() {

        add_theme_support( 'post-thumbnails' );

        // $base = 70;
        // $gutter = 30;

        $width = function ( $columns ) {
            return $this->base * $columns + $this->gutter * ( $columns - 1 );
        };

        foreach ( range( 1, 12 ) as $i ) {

            add_image_size(
                'large_' . $i,
                $width( $i ),
                floor( ( $this->base * $i ) * 0.75 ),
                true
            );

        }


        add_image_size( 'user_profile', $width( 3 ), 350, true );
        add_image_size( 'full_banner', 1170, 250, true );
    }

}
