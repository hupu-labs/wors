<?php
namespace WOR\Models;

class WpQuery implements \IteratorAggregate, \ArrayAccess {

    var $posts = [];

    public function __construct( $plural, $query ) {
        $this->query = $query;
        $this->plural = (String) $plural;
        $this->total = (Integer) $query->found_posts;
        $this->number = (Integer) is( $query->query[ 'posts_per_page' ] );
        $this->page = (Integer) is( $query->query[ 'paged' ] );
        $this->type = (String) $query->query[ 'post_type' ];
    }

    public function first() {
        return $this->posts[0];
    }

    public function to_json() {

        $project_json = [];

        foreach ($this->posts as $p) {
            $project_json[$p->id()] = [
                'id' => $p->id(),
                'title' => $p->title(),
                'location' => $p->meta->location,
                'address' => json_decode($p->meta->address),
                'kml' => $p->meta->kml,
                'gps' => $p->meta->gps,
            ];
        }

        return json_encode( $project_json );

    }

    public function pagination() {

        if ( $this->number === 1 || $this->number === -1 ) {
            return [ 'number' => -1 ];
        }

        $ratio = $this->total / $this->number;

        $out = [
            'pages' => $ratio,
            'plural' => $this->plural,
            'type' => $this->type,
            'total' => $this->total,
            'page' => $this->page,
            'number' => $this->number,
        ];

        if ( $this->total % $this->number === 0 ) {
            return $out;
        } else {
            $out[ 'pages' ] = floor( $ratio + 1 );
            return $out;
        }

    }

    public function add( $wp_post ) {
        $this->posts[] = $wp_post;
    }

    public function getIterator() {
        return new \ArrayIterator( $this->posts );
    }

    public function offsetSet( $id, $value ) {
        $this->posts[ $id ] = $value;
    }

    public function offsetGet( $id ) {
        if ( ! array_key_exists( $id, $this->posts ) ) {
            throw new \InvalidArgumentException( sprintf( 'Form element, "%s", is not defined.', $id ) );
        }

        return $this->posts[ $id ];
    }

    public function offsetExists( $id ) {
        return array_key_exists( $id, $this->posts );
    }

    public function offsetUnset( $id ) {
        unset( $this->posts[ $id ] );
    }
}
