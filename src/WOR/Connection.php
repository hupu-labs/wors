<?php
namespace WOR;

class Connection implements ConnectionInterface {
    public $connection;

    public function set_connection( $connection ) {
        $this->connection = (string) $connection;

        add_action(
            'p2p_init',
            $this->get_connection_func( $connection )
        );
    }

    public function get_connection( $queried_type, $connection ) {
        $connected = new \WP_Query([
            'connected_type' => (string) $connection,
            'connected_items' => $queried_type,
            'nopaging' => true,
        ]);
        return loop( $connected );
    }

    public function get_connection_func( $connection ) {
        return function () use ( $connection ) {
            $conn_array = explode( '_', $connection );
            /* hm( $conn_array[ 0 ] ); */
            /* hm( end($conn_array)); */
            p2p_register_connection_type([
                'name' => $connection,
                'from' => $conn_array[ 0 ] ,
                'to' => end( $conn_array ),
            ]);
        };
    }
}
