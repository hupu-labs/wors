<?php
namespace WOR\Uri;

class Uri {

    public function __construct( $current_uri ) {
        $this->current_uri = $current_uri;
    }

    public function query( $param ) {
        return is( $this->current_uri->query[ $param ] );
    }

}
