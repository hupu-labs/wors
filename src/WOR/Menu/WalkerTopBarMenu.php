<?php
/**
 * Copyright (C) 2013-2015 by Donavan-Ross Costaras <d.costaras@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace WOR\Menu;

class WalkerTopBarMenu extends \Walker {

    public $tree_type = array( 'post_type', 'taxonomy', 'custom' );
    public $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {

        $element->classes = [];

        if ( ! empty( $children_elements[ $element->ID ] ) ) {
            $element->classes[] = 'has-dropdown';
        }

        if ( $element->current || $element->current_item_ancestor ) {
            $element->classes[] = 'active';
        }

        parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );

    }

    function start_el( &$output, $item, $depth = 0, $args = [], $current_object_id = 0 ) {

        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = join( ' ',  array_filter( $classes ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';


        $output .= "<li" . $class_names . ">";

        $atts = array();
        $atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
        $atts['target'] = ! empty( $item->target )     ? $item->target     : '';
        $atts['rel']    = ! empty( $item->xfn )        ? $item->xfn        : '';
        $atts['href']   = ! empty( $item->url )        ? $item->url        : '';

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( ! empty( $value ) ) {
                $value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $output .= '<a'. $attributes .'>' . $item->title . '</a>';

    }

    public function end_el( &$output, $item, $depth = 0, $args = [] ) {
        $output .= "</li>\n";
    }

    function start_lvl( &$output, $depth = 0, $args = [] ) {

        $output .= $this->indent( "<ul class=\"sub-menu dropdown \">\n", $depth );

    }

    function end_lvl( &$output, $depth = 0, $args = [] ) {

        $output .= $this->indent( "</ul>", $depth );

    }

    protected function indent( $input, $depth ) {
        return str_repeat( "\t", $depth ) . $input;
    }
}
