<?php
namespace WOR\Menu;

class WpMenu {

    public function __construct( $name, $walker ) {
        $this->walker = $walker;
        $this->name = (String) $name;

        $this->menu_string = wp_nav_menu([
            'container'      => false,
            'menu_class'     => 'left',
            'theme_location' => $this->name,
            'depth'          => 0,
            'fallback_cb'    => false,
            'walker'         => $this->walker,
            'echo'           => false,
            'items_wrap'     => '%3$s',
        ]);

    }

    public function __toString() {
        return (string) $this->menu_string;
    }

}
