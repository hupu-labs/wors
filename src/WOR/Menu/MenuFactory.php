<?php
namespace WOR\Menu;

class MenuFactory {
    private static $inflector;
    private $menu_locations;

    public function create( $name ) {

        $class = $this->class;

        $walker = $this->walker;
        return new $class( $name, $walker() );

    }

    public function __construct( $walker, $class, $menu_locations ) {

        $this->menu_locations = $menu_locations;
        $this->walker = $walker;
        $this->class = $class;

        foreach ( $menu_locations as $menu_location => $description ) {

            register_nav_menu( $menu_location, $description );

        }
    }

}
