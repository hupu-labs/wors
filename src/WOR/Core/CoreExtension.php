<?php
namespace WOR\Core;
use \WOR\Models\Term;
use \WOR\Widget\WidgetFactory;
use \WOR\Uri\Uri;

use Aura\Router\Map as RouteMap;
use Aura\Router\DefinitionFactory as RouteDefinitionFactory;
use Aura\Router\RouteFactory;
use Aura\Uri\Url\Factory as UrlFactory;
use Aura\Uri\PublicSuffixList;
use Aura\Di\Container;
use Aura\Di\Forge;
use Aura\Di\Config;
use Knp\Snappy\Pdf;

class CoreExtension {

    public function register_with( Container $app ) {

        $app->set( 'term', function () {
            return new Term;
        });

        /* -------------------------------------------------- */
        /* Factories */

        $app->set('widget.area.factory', function () use ( $app ) {
            return $app->newInstance( '\WOR\Widget\WidgetAreaFactory', [
                'inflector' => $app->lazyGet( 'inflector' )
            ]);
        });

        $app->set( 'metabox.factory', function () use ( $app ) {
            return $app->newInstance( '\WOR\Metabox\Factory', [
                'inflector' => $app->get( 'inflector' ),
                'template' => $app->get( 'template' ),
            ]);
        });

        $app->set( 'controller.factory', function () use ( $app ) {
            return $app->newInstance( '\WOR\Controller\Factory', [
                'menu_factory' => $app->get( 'menu.top_bar' ),
                'sidebar_factory' => $app->get( 'widget.area.factory' ),
                'model_factory' => $app->get( 'model.factory' ),
                'theme_options' => $app->get( 'models.theme_options' ),
                'inflector' => $app->get( 'inflector' ),
            ]);
        });

        $app->set( 'wp_post.factory', function () use ( $app ) {
            return $app->newInstance( '\WOR\Models\WpPostFactory', [
                'meta_data_factory' => $app->newInstance( 'WOR\Models\Meta\Factory' ),
                'image_factory' => $app->get( 'image.data.factory' ),
                'inflector' => $app->get( 'inflector' ),
            ]);
        });

        $app->set( 'wp_query.factory', function () use ( $app ) {
                return $app->newInstance( '\WOR\Models\WpQueryFactory', [
                    'inflector' => $app->get( 'inflector' ),
                ]);
            });

        $app->set( 'models.theme_options', function () use ( $app ) {
                return $app->newInstance( '\WOR\Models\ThemeOptions\ThemeOptions' );
            });

        $app->set( 'model.factory', function () use ( $app ) {
            return $app->newInstance( '\WOR\Models\Factory', [
                'wp_post_factory' => $app->get( 'wp_post.factory' ),
                'wp_query_factory' => $app->get( 'wp_query.factory' ),
                'inflector' => $app->get( 'inflector' ),
            ]);
        });

        $app->set( 'menu.top_bar', function () use ( $app ) {
            return $app->newInstance( '\WOR\Menu\MenuFactory', [
                'walker' => $app->newFactory( '\WOR\Menu\WalkerTopBarMenu' ),
                'class' => '\WOR\Menu\WpMenu'
            ]);
        });

        $app->set( 'core', function () use ( $app ) {
            return new Core( $app->get( 'dispatcher' ), $app->get( 'uri' ) );
        });

        $app->set( 'model.gallery.factory', function () use ( $app ) {
            return $app->newInstance( 'WOR\Models\Gallery\Factory', [
                'image_factory' => $app->get( 'image.data.factory' )
            ]);
        });

        $app->set( 'model.image.theme_support', function () use ( $app ) {
                return $app->newInstance( 'WOR\Models\Image\ThemeSupport', [
                    'base' => $app->params[ 'grid_base' ],
                    'gutter' => $app->params[ 'grid_gutter' ],
                ]);
        });


        /* -------------------------------------------------- */
        /* Routing and dispatching */
        $app->set( 'router', function () use ( $app ) {
            return new RouteMap(
                new RouteDefinitionFactory,
                new RouteFactory
            );
        });

        $app->set( 'route', function () use ( $app ) {
            $path = parse_url( $_SERVER[ 'REQUEST_URI' ], PHP_URL_PATH );
            return $app->get( 'router' )->match( $path, $_SERVER );
        });

        $app->set( 'dispatcher', function () use ( $app ) {
            return new Dispatcher(
                $app->get( 'route' ),
                $app->get( 'template' ),
                $app->get( 'controller.factory' )
            );
        });


        $app->set( 'uri.factory', function () use ( $app ) {
            $psl = new PublicSuffixList(
                require $app->params[ 'vendor_dir' ] . '/aura/uri/data/public-suffix-list.php'
            );
            return new UrlFactory( $_SERVER, $psl );
        });


        $app->set( 'uri', function () use ( $app ) {
            return new Uri( $app->get( 'uri.factory' )->newCurrent() );
        });

        /* -------------------------------------------------- */
        /* Widgets */
        $app->set( 'widget.factory', function () use ( $app ) {
            return new WidgetFactory(
                $app->get( 'template' ),
                $app->get( 'model.factory' )
            );
        });

    }

    /* function admin() { */
    /*     if ( current_user_can( 'edit_theme_options' ) ) { */
    /*         if ( $this[ 'request' ]->action_is( 'update' ) ) { */
    /*             update_option( */
    /*                 'wor_theme_options', */
    /*                 $this[ 'request' ]->get( 'wor_theme_options' ) */
    /*             ); */
    /*         } */
    /*     } */
    /* } */
}
