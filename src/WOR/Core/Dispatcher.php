<?php
namespace WOR\Core;

class Dispatcher {
    private $route;
    private $template;
    private $render_params;
    private $controller_factory;

    public function __construct( $route, $template, $controller_factory ) {

        $this->route = $route;
        $this->template = $template;
        $this->controller_factory = $controller_factory;

        if ( ! is_admin() ) {
            $pn = $GLOBALS['pagenow'];
            if ( $pn != 'wp-login.php' && $pn != 'wp-register.php' ) {
                add_action( 'init', [ $this, 'route' ], 2 );
                add_action( 'wp', [ $this, 'dispatch' ], 1 );
            }
        }

    }

    public function route() {

        if ( $this->route ) {

            $controller_name = $this->route->values[ 'controller' ];
            $this->controller = $this->controller_factory->create( $controller_name );

            $this->method = $this->route->values[ 'method' ];
            $this->params = $this->route->matches;

        } else {
            throw new \Exception( 'No route found' );
        }

    }

    public function dispatch() {
        $this->template->render( $this->exec() );
    }

    private function run_if_method_exists( $method ) {
        if ( method_exists( $this->controller, $method ) ) {
            $this->controller->$method();
        }
    }

    public function exec() {

        global $wp_query;

        $this->run_if_method_exists( 'before' );

        $method = $this->method;

        if ( empty( $this->params ) ) {
            $render_params = $this->controller->$method();
        } else {
            $render_params = $this->controller->$method( $this->params );
        }

        if ( empty( $render_params ) ) {

            status_header( 404 );
            $wp_query->set_404();

            $render_params = $this->controller_factory->create( 'site' )->http_404();

        } else {

            status_header( 200 );
            $wp_query->is_404 = false;

        }

        $this->run_if_method_exists( 'after' );

        $render_params[ 'route' ] = $this->params;
        return $render_params;

    }

}
