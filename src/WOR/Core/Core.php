<?php
namespace WOR\Core;
class Core {

    public function __construct( $dispatcher, $uri_factory ) {

        $this->dispatcher = $dispatcher;
        $this->uri_factory = $uri_factory;
        $this->add_and_remove_actions();

    }

    private function add_and_remove_actions() {
        remove_all_actions( 'template_redirect' );
        add_action( 'pre_get_posts', [ $this, 'remove_main_query' ] );
        add_filter( 'preview_post_link', [ $this, 'preview_link' ]);
        // Clean up the head
        /* remove_action('wp_head', 'rsd_link'); */
        /* remove_action('wp_head', 'wlwmanifest_link'); */
        /* remove_action('wp_head', 'wp_generator'); */
        /* remove_action('wp_head', 'wp_shortlink_wp_head'); */

        /* add_action( 'init', self::wor_init(), 1 ); */
        /* add_action( 'init', self::wor_ready(), 15 ); */
        /* add_filter( 'query_vars', self::add_query_vars() ); */

        add_action( 'widgets_init',  [ $this, 'unregister_default_widgets' ], 11 );
    }


function preview_link( $permalink ) {
    global $post;

    $new_url = (
        get_bloginfo('url') .
        '/preview/'
    );

    return $new_url;
}
    // unregister all widgets
    public static function unregister_default_widgets() {
        unregister_widget('WP_Widget_Pages');
        unregister_widget('WP_Widget_Calendar');
        unregister_widget('WP_Widget_Archives');
        unregister_widget('WP_Widget_Links');
        unregister_widget('WP_Widget_Meta');
        unregister_widget('WP_Widget_Search');
        unregister_widget('WP_Widget_Categories');
        unregister_widget('WP_Widget_Recent_Posts');
        unregister_widget('WP_Widget_Recent_Comments');
        unregister_widget('WP_Widget_RSS');
        unregister_widget('WP_Widget_Tag_Cloud');
        unregister_widget('WP_Nav_Menu_Widget');
    }


    public static function remove_main_query( $query ) {
        if ( is_admin() ) {
            return $query;
        } else {
            if ( $query->is_main_query() || $query->is_home() ) {
                return false;
            } else {
                return $query;
            }
        }
    }

    private static function wor_init() {
        return function () {
            do_action('wor_preload');
            do_action('wor_init');
        };
    }

    /* private static function wor_ready() { */
    /*     return function () {  */
    /*         do_action('wor_ready'); */
    /*     }; */
    /* } */

    /* private static function add_query_vars() { */
    /*     return function ( $vars ) {  */
    /*         $vars[] = 'wor_controller'; */
    /*         $vars[] = 'wor_method'; */
    /*         $vars[] = 'wor_parameters'; */
    /*         return $vars; */
    /*     }; */
    /* } */

}
