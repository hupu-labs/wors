<?php
namespace WOR\Widget;

class WidgetAreaFactory {
    protected static $areas;

    public function __construct( $inflector, $widget_areas ) {

        $this->inflector = $inflector;
        $this->widget_areas = $widget_areas;

        if ( ! empty( $widget_areas ) ) {
            foreach ( $widget_areas as $name ) {
                add_action( 'widgets_init', $this->get_register_func( $name ) );
            }
        }

    }

    public function get_widget_areas() {
        return $this->widget_areas;
    }

    public function create( $name ) {
        return new WidgetArea( $name );
    }

    private function get_register_func( $name ) {
        return function () use ( $name ) {
            register_sidebar([
                'id' => $name,
                'name' => $name,
                'description' => 'The ' . $name . ' widget area.',
                'before_widget' => '',
                'after_widget' => '',
                'before_title' => '<h4 class="widget-title">',
                'after_title' => '</h4>',
            ]);
        };
    }

}
