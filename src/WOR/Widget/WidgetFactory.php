<?php
namespace WOR\Widget;

class WidgetFactory {
    protected static $template;
    protected static $model_factory;

    public function __construct( $template, $model_factory ) {
        self::$template = $template;
        self::$model_factory = $model_factory;
    }

    public static function create( $args ) {

        $args[ 'template_environment' ] = self::$template;
        $args[ 'model_factory' ] = self::$model_factory;

        $class = $args[ 'class' ];
        unset( $args[ 'class' ] );

        $reflection = new \ReflectionClass( $class );
        $params = $reflection->getConstructor()->getParameters();

        foreach ( $params as $param ) {

            if ( ! isset( $args[ $param->name ] ) ) {
                throw new \InvalidArgumentException("$class requires an argument '$param->name'.");
            }

            $ordered_params[] = $args[ $param->name ];

        }

        global $wp_widget_factory;
        $wp_widget_factory->widgets[ $args[ 'name' ] ] = $reflection->newInstanceArgs( $ordered_params );

    }

}
