<?php

namespace WOR\Widget;

interface WidgetInterface {
    function wor_widget();
    function wor_form();
}
