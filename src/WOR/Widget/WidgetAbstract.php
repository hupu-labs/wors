<?php
namespace WOR\Widget;
use \Functional as F;

abstract class WidgetAbstract extends \WP_Widget {
    protected $form;
    protected $description;
    protected $template;
    private static $template_environment;


    public function __construct(
        $name,
        $description,
        \WOR\Form\FormInterface $form,
        $template,
        \WOR\Template\TemplateEnvironmentInterface $template_environment,
        $model_factory
    ) {

        self::$template_environment = $template_environment;

        parent::__construct( sanitize_title( $name ), $name, [
            'classname' => 'widget_' . sanitize_title( $name ),
            'description' => $description
        ]);

        $this->template = (String) $template;
        $this->name = (String) $name;
        $this->description = (String) $description;
        $this->form = $form->set_prefix( 'widget-' . $this->id_base );
        $this->model_factory = $model_factory;

    }

    public function wor_widget() {
        throw new Exception('Function wor_widget needs to be implemented in child class');
    }

    public function wor_form() {
        return (String) $this->form;
    }

    public function widget( $args, $instance ) {

        foreach ( $this->form->set_values( $instance ) as $key => $ele ) {
            $this->$key = $ele->value();
        }

        echo self::$template_environment->render( $this->wor_widget() );

    }

    public function update( $new_instance, $old_instance ) {
        return $this->form->set_values( $new_instance )->to_array();
    }


    public function form( $instance ) {

        $this->form->set_values( $instance )
                   ->set_index( $this->number );

        echo $this->wor_form();

    }
}
