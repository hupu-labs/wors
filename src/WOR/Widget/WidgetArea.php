<?php
namespace WOR\Widget;

class WidgetArea {
    var $name;

    public function __construct( $name ) {
        $this->name = $name;
        $this->content = self::loop( $this->name );
    }

    public static function loop( $name ) {
        global $wp_registered_sidebars, $wp_registered_widgets;

        $index = sanitize_title( $name );
        foreach ( (array) $wp_registered_sidebars as $key => $value ) {
            if ( sanitize_title( $value[ 'name' ] ) == $index ) {
                $index = $key;
                break;
            }
        }

        $sidebars_widgets = wp_get_sidebars_widgets();
        if ( empty( $wp_registered_sidebars[ $index ] ) || empty( $sidebars_widgets[ $index ] ) || ! is_array( $sidebars_widgets[ $index ] ) ) {
            return false;
        }

        $sidebar = $wp_registered_sidebars[$index];

        $did_one = false;
        $widgets = [];

        foreach ( (array) $sidebars_widgets[$index] as $id ) {

            if ( !isset($wp_registered_widgets[$id]) ) continue;

            $params = array_merge(
                array( array_merge( $sidebar, array('widget_id' => $id, 'widget_name' => $wp_registered_widgets[$id]['name']) ) ),
                (array) $wp_registered_widgets[$id]['params']
            );

            // Substitute HTML id and class attributes into before_widget
            $classname_ = '';
            foreach ( (array) $wp_registered_widgets[$id]['classname'] as $cn ) {
                if ( is_string($cn) )
                    $classname_ .= '_' . $cn;
                elseif ( is_object($cn) )
                    $classname_ .= '_' . get_class($cn);
            }
            $classname_ = ltrim($classname_, '_');

            $params[0]['before_widget'] = sprintf($params[0]['before_widget'], $id, $classname_);

            $params = apply_filters( 'dynamic_sidebar_params', $params );

            $callback = $wp_registered_widgets[$id]['callback'];

            do_action( 'dynamic_sidebar', $wp_registered_widgets[$id] );

            if ( is_callable($callback) ) {
                ob_start();
                call_user_func_array($callback, $params);
                $widgets[] = ob_get_clean();
                $did_one = true;
            }
        }

        $count = count($widgets);
        if ( $count == 0 ) {
            return false;
        } else {
            return  [ 'list' => $widgets, 'length' => $count, 'columns' => floor( 12 / $count ) ];
        }
    }
}
