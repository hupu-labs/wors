<?php
namespace WOR\Customiser;

class Section {

    public function __construct( $title, $description, $form, $control_factory, $capability, $get_js ) {

        $this->title = $title;
        $this->name = sanitize_title( $title );
        $this->capability = $capability;
        $this->description = $description;
        $this->control_factory = $control_factory;
        $this->get_js = $get_js;

        $this->form = $form->set_prefix( $this->name . '_form' )
                           ->set_decorator( 'CustomizerDecorator' );

        $this->add_actions();

    }

    private function add_actions() {
        add_action( 'customize_register', $this->create_action() );
    }

    private function create_action() {

        return function ( $wp_customize ) {

            if ( $wp_customize->is_preview() && ! is_admin() ) {

                $script_tag = (
                    '<script type="text/javascript">' .
                    file_get_contents( get_stylesheet_directory() . $this->get_js ) .
                    '</script>'
                );

                add_action( 'wp_footer', function () use ( $script_tag ) { echo $script_tag; }, 21);

            }

            $section_name = $this->name . '_section';
            $wp_customize->add_section(
                $section_name, [
                    'title'       => $this->title,
                    'capability'  => $this->capability,
                    'description' => $this->description,
                ]
            );

            foreach ( $this->form as $form_element ) {

                $setting_link = "wors_options[{$form_element->slug()}]";

                $wp_customize->add_setting( $setting_link, [
                    'capability' => 'edit_theme_options',
                    'type'       => 'option',
                    'default'    => $form_element->standard(),
                    'transport'  => 'postMessage',
                ]);

                $form_element->set_customizer_setting( $setting_link );

                $control = $this->control_factory->create([
                    'form_element' => $form_element,
                    'settings'     => $setting_link,
                    'manager'      => $wp_customize,
                    'section_name' => $section_name,
                ]);

                $wp_customize->add_control( $control );

            }

            return $wp_customize;

        };

    }

}
