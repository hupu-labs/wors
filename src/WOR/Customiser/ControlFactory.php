<?php
namespace WOR\Customiser;

class ControlFactory {

    public function create( $args ) {
        return new Control(
            $args[ 'form_element' ],
            $args[ 'settings' ],
            $args[ 'manager' ],
            $args[ 'section_name' ]
        );
    }

}
