<?php
namespace WOR\Customiser;

class SectionFactory {

    public function __construct( $control_factory ) {
        $this->control_factory = $control_factory;
    }

    public function create( $args ) {

        return new Section(
            $args[ 'title' ],
            $args[ 'description' ],
            $args[ 'form' ],
            $this->control_factory,
            is( $args[ 'capability' ], 'edit_theme_options' ),
            is( $args[ 'get_js_src' ] )
        );

    }

}
