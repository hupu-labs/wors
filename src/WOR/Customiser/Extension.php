<?php
namespace WOR\Customiser;

class Extension {

    public function register_with( $app ) {

        $app->set( 'customiser.control.factory', function () use ( $app ) {
                return new ControlFactory;
            });
        $app->set( 'customiser.section.factory', function () use ( $app ) {
                return new SectionFactory( $app->get( 'customiser.control.factory' ) );
            });
    }
}
