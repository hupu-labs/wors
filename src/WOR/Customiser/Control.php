<?php
namespace WOR\Customiser;

class Control extends \WP_Customize_Control {

    public function __construct(
        $form_element,
        $setting_link,
        $wp_customize,
        $section_name
    ) {

        $this->form_element = $form_element;
        $this->setting_link = $setting_link;

        // TODO fix
        $this->type = 'select';

        parent::__construct( $wp_customize, $setting_link, [
            'section' => $section_name,
            'settings' => $setting_link,
        ]);

    }

    public function __call( $name, $args ) {
        return $this->form_element->$name( $args );
    }

    public function render_content(){
        echo $this->form_element;
    }

    public function enqueue() {
        $this->form_element->enqueue();
    }

}
