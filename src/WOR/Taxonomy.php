<?php
namespace WOR\Form\Element\Option\Select;

class Taxonomy extends Base {

    public function __construct( $args ) {
        $args[ 'options' ] = get_all_terms_from_taxonomy( $args[ 'name' ] );
        parent::__construct( $args );
    }

}
