<?php
namespace WOR\Api\Facebook;

class FacebookExtension {

    public function register_with( \Aura\Di\Container $app ) {

        $app->set( 'facebook.sdk', function () use ( $app ) {
            return new \Facebook( [
                'appId' => $app->params[ 'facebook' ][ 'app_id' ],
                'secret' => $app->params[ 'facebook' ][ 'secret' ],
                'cookie' => true
            ] );
        });

        $app->set( 'facebook', function () use ( $app ) {
            return new Facebook(
                $app->get( 'facebook.sdk' ),
                $app->params[ 'facebook' ][ 'home_url' ],
                $app->params[ 'facebook' ][ 'app_id' ],
                $app->params[ 'facebook' ][ 'secret' ],
                $app->params[ 'facebook' ][ 'access_token' ],
                $app->get( 'cache.transient' ),
                $app->get( 'uri.factory' )
            );
        });



    }
}
