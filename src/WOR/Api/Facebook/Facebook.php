<?php
namespace WOR\Api\Facebook;

use \Guzzle\Http\Client;

class Facebook extends Client {

    function __construct(
        \Facebook $sdk,
        $home_url,
        $app_id,
        $secret,
        $access_token,
        \WOR\Cache\Transient $transient,
        $uri_factory
    ) {
        $this->sdk = $sdk;
        $this->home_url = '/' . $home_url . '/';
        $this->app_id = $app_id;
        $this->secret = $secret;
        $this->access_token = $access_token;
        $this->transient = $transient;
        $this->uri_factory = $uri_factory;
        $this->sdk->setAccessToken( $this->access_token );
        $this->posts = $this->posts()[ 'data' ];
    }

    public function posts() {

        return $this->transient->create(
            'facebook_feed',
            function () {

                $posts = $this->sdk->api( $this->home_url . 'posts', 'GET', [
                    'limit' => 25,
                ]);

                foreach ( $posts[ 'data' ] as &$post ) {
                    if ( isset( $post[ 'object_id' ] ) ) {
                        $post[ 'object' ] = $this->sdk->api( $post[ 'object_id' ], 'GET' );
                    }
                }

                return $posts;
            }
        );

    }

    public function with_types( $types ) {

        $this->posts = array_filter(
            $this->posts,
            function ( $e ) use ( $types ) {
                foreach ( $types as $type ) {
                    if ( $e[ 'type' ] === $type ) {
                        return $e;
                    }
                }
            }
        );
        return $this;

    }

    public function without_status_types( $status_types ) {

        $this->posts = array_filter(
            $this->posts,
            function ( $e ) use ( $status_types ) {
                foreach ( $status_types as $status_type ) {
                    if ( $e[ 'type' ] === 'link' || $e[ 'status_type' ] !== $status_type ) {
                        return $e;
                    }
                }
            }
        );
        return $this;
    }

    /* public function with_types_filter() { */
    /*     $types = $this->query[ 'types' ]; */
    /*     $filtered_array = array_filter( */
    /*         $this->posts()[ 'data' ], */
    /*         function ( $e ) use ( $types ) { */
    /*             foreach ( $types as $type ) { */
    /*                 if ( $e[ 'type' ] === $type ) { */
    /*                     return $e; */
    /*                 } */
    /*             } */
    /*         } */
    /*     ); */
    /* } */

    public function loop( $number ) {
        return array_slice($this->posts, 0, $number);
    }
}
