<?php
namespace WOR\Api\Twitter;
//use
class AppExtension {

    public function register_with( \Aura\Di\Container $app ) {

        $app->set( 'autolink', function () {
            return new \Twitter_Autolink;
    });


        $app->set( 'autolink.twig_extension', function () use ( $app ) {
        return new TwigExtension( $app->get( 'autolink' ) );
    });
    }

}
