<?php
namespace WOR\Api\Twitter;

class TwigExtension extends \Twig_Extension {

    public function __construct( $autolink ) {
        $this->autolink = $autolink;
    }

    public function getFilters() {
        return [
            'auto_link' => new \Twig_Filter_Method( $this, 'auto_link', [
                'is_safe' => [ 'html' ]
            ]),
            'no_follow' => new \Twig_Filter_Method( $this, 'no_follow' )
        ];
    }

    public function getName() {
        return "twitter_twig_extension";
    }

    public function auto_link( $string ) {
        return $this->autolink->autoLink( $string );
    }

    public function no_follow( $string ) {
        $string = str_replace('href=', 'rel="nofollow" href=', $string);
        return $string;
    }
}
