<?php
use \Functional as F;

define( 'PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'PLUGIN_FILE', __DIR__ );

function last( $array ) {
    return array_slice( $array, -1 )[0];
}

function meta( $field ) {
    return new \WOR\Query\Meta\Base( $field );
}

function date_meta( $field ) {
    return new \WOR\Query\Meta\DateTime( $field );
}

function post_date() {
    return new \WOR\Query\Date\Base();
}

function is( &$value, $default = null ) {
    return isset( $value ) ? $value : $default;
}

function not_empty( $ele ) {
    return ! empty( $ele );
}

function concat_string( $string, $index, $collection, $initial ) {
    return $initial . $string;
}

function head( $array ) {
    return F\head( $array );
}

function human_filesize($bytes, $decimals = 2) {
    $size = ['B','kB','MB','GB','TB','PB','EB','ZB','YB'];
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
}

add_filter(
    'xmlrpc_methods',
    function( $methods ) {
        unset( $methods['pingback.ping'] );
        return $methods;
    }
);

function get_featured( $post_id ) {

    $path = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'full' );

    if ( $path ) {
        return F\head( $path );
    } else {
        return false;
    }

}

function filter_tag_link($taglink, $tag, $taxonomy) {
    $old_url = get_bloginfo('url');
    $new_url = get_bloginfo('url') . '/articles';
    if ('post_tag' == $taxonomy) {
        $taglink = str_replace($old_url, $new_url, $taglink);
    }
    return $taglink;
}

add_filter('term_link', 'filter_tag_link', 10, 3);

function change_permalinks($permalink, $post, $leavename) {
    $old_url = get_bloginfo('url');
    $new_url = get_bloginfo('url') . '/article';
    $new_permalink = str_replace($old_url, $new_url, $permalink);
    return $new_permalink;
}

add_filter( 'post_link', 'change_permalinks', 10, 3);

function change_category_permalinks($permalink, $id ) {
    $old_url = get_bloginfo('url');
    $new_url = get_bloginfo('url') . '/articles';
    $new_permalink = str_replace($old_url, $new_url, $permalink);
    return $new_permalink;
}
add_filter( 'category_link', 'change_category_permalinks', 10, 3);

add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
    $existing_mimes['pptx'] = 'application/application/vnd.openxmlformats-officedocument.presentationml.presentation';
    $existing_mimes['kml'] = 'application/vnd.google-earth.kml+xml';
    return $existing_mimes;
}

function get_the_slug() {
    global $post;
    return $post->post_name;
}

function new_excerpt_more($more) {
    return '&hellip;';
}
add_filter('excerpt_more', 'new_excerpt_more');

function get_the_meta( $id ) {
    return array_map(
        function ( $e ) { return $e[ 0 ]; },
        get_post_meta( $id )
    );
}

function get_id_from_slug( $page_slug ) {
    $page = get_page_by_path( $page_slug );
    if ( $page ) {
        return $page->ID;
    } else {
        return null;
    }
}

function wp_get_attachment( $attachment_id ) {
    $attachment = get_post( $attachment_id );
    return array(
        'alt' => get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true ),
        'caption' => $attachment->post_excerpt,
        'description' => $attachment->post_content,
        'href' => get_permalink( $attachment->ID ),
        'src' => $attachment->guid,
        'title' => $attachment->post_title
    );
}

function get_image_size( $name ) {
    global $_wp_additional_image_sizes;

    if ( isset( $_wp_additional_image_sizes[$name] ) )
        return $_wp_additional_image_sizes[$name];

    return false;
}

function wp_get_attachments( $post_id ) {
    $args = [
        'post_type'   => 'attachment',
        'numberposts' => -1,
        'post_status' => 'any',
        'post_parent' => $post_id,
    ];

    $attachments = get_posts( $args );

    return $attachments;
}

function get_all_terms_from_taxonomy( $taxonomy ) {

    $terms = [];

    foreach ( get_terms( $taxonomy, 'hide_empty=0' ) as $term ) {
        $terms[ $term->slug ] = $term->name;
    }

    return $terms;

}

function get_all_categories() {
    return get_all_terms_from_taxonomy( 'category' );
}

function get_term_id_from_slug( $slug, $taxonomy ) {
    return get_term_by( 'slug', $slug, $taxonomy )->term_id;
}

function get_post_categories( $id ) {
    $post_categories = wp_get_post_categories( $id );
    $cats = [];
    foreach( $post_categories as $c ) {
        $cat = get_category( $c );
        $cats[] = array( 'name' => $cat->name, 'slug' => $cat->slug );
    }
    return $cats;
}

function capture( $func, $args = false ) {

    ob_start();

    if ( $args ) {
        call_user_func( $func, $args );
    } else {
        $func();
    }

    return ob_get_clean();

}

function pp( $code, $output = true ) {

    if ( $output ) {
        echo '<style> .hm_debug { word-wrap: break-word; white-space: pre; text-align: left; position: relative; background-color: rgba(0, 0, 0, 0.8); font-size: 11px; color: #a1a1a1; margin: 10px; padding: 10px; margin: 0 auto; width: 80%; overflow: auto;  -moz-border-radius: 5px; -webkit-border-radius: 5px; text-shadow: none; } </style> <br /> <pre class="hm_debug">';
    }

    if ( ! is_array( $code ) && ! is_object( $code ) ) :

        if ( $output )
            var_dump( $code );

        else
            var_export( $code, true );

    else :

        if ( $output )
            print_r( $code );

        else
            print_r( $code, true );
    endif;

    if ( $output )
        echo '</pre><br />';

    return $code;

}



add_action(
    'admin_enqueue_scripts',
    function () {

        wp_register_script(
            'admin_js',
            PLUGIN_URL . 'public/admin.min.js',
            [
                'jquery',
                'jquery-ui-core',
                'jquery-ui-datepicker',
                'jquery-ui-slider'
            ],
            false,
            true
        );

        wp_register_style(
            'admin_css',
            PLUGIN_URL . 'public/style.admin.min.css'
        );

        wp_register_style(
            'jquery-ui',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css'
        );

        wp_enqueue_style( 'jquery-ui' );

        wp_enqueue_style( 'admin_css' );
        wp_enqueue_script( 'admin_js' );

    }
);

add_action(
    'customize_enqueue_scripts',
    function () {

        wp_register_script(
            'customize_js',
            PLUGIN_URL . 'public/customize.min.js',
            false,
            true
        );

        wp_enqueue_script( 'customize_js' );

    }
);

add_action(
    'wp_enqueue_scripts',
    function () {

        // wp_enqueue_script( 'jquery' );

        // wp_register_script(
        //     'app_js',
        //     PLUGIN_URL . 'public/app.min.js',
        //     ['jquery'],
        //     false,
        //     true
        // );
        // wp_enqueue_script( 'app_js' );

        // wp_register_style(
        //     'app_css',
        //     PLUGIN_URL . 'public/style.min.css'
        // );
        // wp_enqueue_style( 'app_css' );

    }
);
