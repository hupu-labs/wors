export BASE_PATH ?= $(PWD)
export DOCKER_TASK := docker run --rm -it
export IMAGE := $(IMAGE_NAME):$(VERSION)

export SHA := $(shell git rev-parse --verify HEAD)

export CLOUD_PROJECT ?= roll-161014
export GCLOUD_COMPUTE_ZONE_DOMAIN ?= eu
export GCLOUD_COMPUTE_ZONE ?= europe-west1-b

export EXTERNAL_REGISTRY_ENDPOINT ?= $(GCLOUD_COMPUTE_ZONE_DOMAIN).gcr.io
export REGISTRY_URL ?= $(EXTERNAL_REGISTRY_ENDPOINT)/$(CLOUD_PROJECT)
export GCLOUD_SERVICE_KEY ?= $(shell pass hupu/google/wors-roll/service-account-base64)

export GCLOUD ?= gcloud

export all-makefiles := \
    ops/Makefile

export docker := wors

include $(all-makefiles)
