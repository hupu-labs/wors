latlng = new google.maps.LatLng -24.357335,30.947203

options =
  zoom: 15
  center: latlng
  mapTypeId: google.maps.MapTypeId.ROADMAP

map = new google.maps.Map document.getElementById( 'map-canvas' ), options

marker = new google.maps.Marker
  position: latlng
  map: map
  title: 'My workplace'

infowindow = new google.maps.InfoWindow
  content:
    '<div id="map-info" style="width:100%;background-color:#0f0;">' +
    '<h3>AWARD‎</h3>' +
    '<br><span>' +
    '<b>Address:</b><br>' +
    'No 14 Safari Junction<br>' +
    'Hoedspruit' +
    '</span>' +
    '</div>'

google.maps.event.addListener marker, 'click', () ->
  infowindow.open map, marker
