jQuery ($) ->

  do ->
    ($ '.duration-picker').timepicker
      timeFormat: 'HH:mm:ss'
      minuteGrid: 10

  do ->

    ($ '.gallery_sortable').sortable
      update: ->
        $lis = $ '.gallery_sortable li img'
        new_val = ( id: e.id, url: e.src for e in $lis )
        ($ '.gallery-input').val JSON.stringify( new_val )


    ($ '.wp_upload_custom').click () ->
      $button = $ @
      $ul = $button.siblings '.gallery_sortable'
      $field = $ '.gallery-input'
      editor = wp.media
        className: 'media-frame gallery-file-frame'
        button:
          text: 'Update gallery'
        multiple: true
        title: 'Select images for the gallery:'
        library:
          type: 'image'

      editor.off 'select'

      editor.on 'open', ->
        selection = editor.state().get('selection')
        meta = $.parseJSON $field.val()
        meta_ids = (datum.id for datum in meta)
        for id in meta_ids
          attachment = wp.media.attachment( id )
          attachment.fetch()
          selection.add( if attachment then [ attachment ] else [] )

      editor.on 'select', ->
        selections = editor.state().get( 'selection' ).toJSON()

        ids = ( id: e.id, url: e.url, caption: e.caption for e in selections )
        items = ( '<li><img id="#{e.id}" class="gallery_image" src="#{e.url}"></li>' for e in ids )

        $ul.empty()
        $ul.append items.join('')

        $field.val JSON.stringify( ids )

      editor.open()

  ($ '.custom_upload_image_button').click ->

    obj = $(this)
    formfield = obj.siblings '.custom_upload_image'
    tb_show('', 'media-upload.php?type=file&TB_iframe=true')

    window.send_to_editor = (html) ->
      formfield.val $(html).attr('href')
      tb_remove()
      false

  ($ '.custom_clear_image_button').click ->
    parent = $(this).parent()
    parent.siblings('.custom_upload_image').val ''
    false

  ($ '.repeatable-add').click ->

    # Get the tag enclosing the add button.
    enclosing_tag = ($ @).parent()

    # Find the last li...
    last_li = enclosing_tag.find 'li.repeatable:last'

    # ... and clone it.
    clone = last_li.clone true

    # Get the input of the cloned li and empty the value.
    clones_empty_input = ($ 'input', clone).val('')
    clones_empty_img = ($ 'img', clone).attr('src', '')

    # Set the name attr of the clone's input to...
    clones_empty_input.attr 'name', (index, name) ->

      # ... the current name with the digits replaced with...
      name.replace /\d([^\d]*$)/, (match, group) ->

        # ... the length of li elements in the enclosing tag. Because the array
        # index starts at 0 the length is always one more.
        ($ 'li', enclosing_tag).length + group

    # insert the clone after the last li element.
    clone.insertAfter last_li, enclosing_tag

    # return false to prevent the anchor from firing.
    false

  ($ '.repeatable-remove').click ->

    # Find the closest parent li relative to the clicked remove button and remove it.
    ($ @).closest('li').remove()

    # return false as it's an anchor
    false

  ($ '.custom_repeatable .item-edit' ).click ->
    closest_repeatable = ($ @).closest '.repeatable'
    closest_settings = closest_repeatable.find '.menu-item-settings'
    closest_settings.toggle 'slow'
    false

  ($ '.repeatable-open-all' ).click ->
    all_repeatables = ($ '.repeatable')
    all_settings = all_repeatables.find '.menu-item-settings'
    all_settings.show 'slow'
    ($ @).hide()
    ($ '.repeatable-close-all').show()
    false

  ($ '.repeatable-close-all' ).click ->
    all_repeatables = ($ '.repeatable')
    all_settings = all_repeatables.find '.menu-item-settings'
    all_settings.hide 'slow'
    ($ @).hide()
    ($ '.repeatable-open-all').show()
    false

  ($ '.custom_repeatable').sortable
    opacity: 0.6,
    revert: true,
    cursor: 'move',
    handle: '.sort'


  do ->
    hidden_check = ($ 'input.hidden_check')

    bind_click = ( element, closest ) ->
      element.click ->
        if element.attr 'checked'
          closest.show 'fast'
        else
          closest.hide 'fast'

    hidden_check.map ->
      $this = $(this)
      closest = $this.closest('form').find '.hidden_field'
      if @.checked
        closest.show 'fast'
      bind_click $this, closest

  $('.date-time-picker').AnyTime_picker
    placement: 'inline'
    hideInput: true
    firstDOW: 1

  $('.date-picker').AnyTime_picker
    placement: 'inline'
    hideInput: true
    format: "%Y/%c/%e"
    firstDOW: 1

  $('.time-picker').AnyTime_picker
    placement: 'inline'
    hideInput: true
    format: "%H:%i"

  $('.multiple-select').select2()

  # GMAP form element
  do ->
    $address_input = ($ '.address_input_select' )

    value = $.parseJSON $address_input.val()

    change_marker = ( lat_lng, map ) ->
      marker = new google.maps.Marker position: lat_lng
      marker.setMap map
      map.setCenter( lat_lng )

    create_map = ( lat, lng ) ->
      canvas = $ '#map-canvas'
      canvas.show()
      canvas.attr 'style', 'width: 400px; height: 300px;'
      lat_lng = new google.maps.LatLng( lat, lng )
      map = new google.maps.Map canvas[ 0 ],
        mapTypeId: google.maps.MapTypeId.ROADMAP
        center: new google.maps.LatLng( lat_lng )
        zoom: 16
      change_marker lat_lng, map
      return map

    map = if value then create_map value.lat, value.lng else false

    timer = null

    gmap_request_factory = (query) ->
      clearTimeout timer
      timer = setTimeout(->

        geocoder = new google.maps.Geocoder()
        data = results: null
        geocoder.geocode address: query.term, (results, status) ->

          data.results = for key, value of results
            id: JSON.stringify
              address: value.formatted_address
              lat: value.geometry.location.lat()
              lng: value.geometry.location.lng()
            text: value.formatted_address
            location: value.geometry.location
          query.callback data

      ,1000)


    $(".address_input_select").select2
      placeholder: "Search for an address",
      minimumInputLength: 5
      query: (query) ->
        gmap_request_factory query
      initSelection: (element, callback) ->
        val = $.parseJSON element.val()
        callback id: val, text: val.address

    ($ '.address_input_select' ).on 'change', ->
      val = $.parseJSON this.value

      if map
        lat_lng = new google.maps.LatLng( val.lat, val.lng )
        change_marker lat_lng, map
      else
        map = create_map( val.lat, val.lng )
