module.exports = (grunt) ->

  # load all grunt tasks matching the `grunt-*` pattern
  require("load-grunt-tasks") grunt
  grunt.initConfig

    # watch for changes and trigger compass, jshint, uglify and livereload
    watch:

      compass:
        files: ["sass/*.sass"]
        tasks: ["compass","cssmin:dist"]

      coffee:
        files: "coffee/**/*.coffee"
        tasks: ["coffee", "uglify:admin"]

      coffeelint:
        files: ["coffee/**/*.coffee", "Gruntfile.coffee"]

    # compass and scss
    compass:
      dist:
        options:
          httpPath: "/"
          importPath: [
            "vendor/zurb/foundation/scss"
          ]
          sassDir: "sass"
          cssDir: "dist/css"
          outputStyle: "nested"

    cssmin:
      dist:
        files:
          'public/style.admin.min.css': [
            'bower_components/spectrum/spectrum.css'
            'bower_components/select2/select2.css'
            'bower_components/jqueryui-timepicker-addon/src/jquery-ui-timepicker-addon.css'

            'dist/css/wors.app.css'
          ]

          'public/style.min.css': [
            'dist/css/wors.app.css'
          ]

    coffee:
      compile:
        files:

          'dist/js/app.js': [
            'coffee/slick.coffee'
            'coffee/app.coffee'
          ]

          'dist/js/admin.js': ['coffee/admin.coffee']

          'dist/js/customize.js': [
            'coffee/colour_scheme_element.coffee'
          ]

    uglify:
      options:
        mangle: false
        beautify: true
      admin:
        files:

          'public/app.min.js': [
            'bower_components/slick-carousel/slick/slick.js'

            'dist/js/app.js'
          ]

          'public/admin.min.js': [
            'bower_components/ama3-anytime/anytime.js'
            'bower_components/select2/select2.js'
            'bower_components/jqueryui-timepicker-addon/dist/jquery-ui-timepicker-addon.js'
            'bower_components/pleasejs/dist/Please.js'
            'bower_components/spectrum/spectrum.js'

            'dist/js/admin.js'
          ]

          'public/customize.min.js': [
            'dist/js/customize.js'
          ]

  grunt.registerTask "css", ["cssmin:dist"]

  # register task
  grunt.registerTask "default", ["watch"]
  grunt.registerTask "concat", ["concat"]
